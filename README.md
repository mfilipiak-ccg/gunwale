# Gunwale

This project is the result of PaaS and DevSecOps pipeline explorations. It seeks to make recommendations for providing easy to use and easy to maintain integration environments for projects developed independently from one another.

A book stitching the various pieces of documentation together is available.

https://mfilipiak-ccg.gitlab.io/gunwale

## Tenents

* The environment can be distributed to all participating development teams for testing prior to delivery
* The pipeline will perform final validation on all code
  * Producing reports that are clear about what is/isn't checked per project
  * Have a consistent sequence of steps followed for particular classes of projects
* The pipeline will perform an automated deploy of all artifacts
* The pipeline will offer clear path to including integration tests for all projects to validate success on a fully integrated system prior to deployment rollout
* Projects can be delivered and deployed on an environment without an internet connection
* The runtime will encourage use of the Zero Trust model
* The runtime will support at least basic service discovery
* The runtime will support horizontal scaling
* The runtime will support at least basic secret and configuration management


Interested in working with us?
Check us out at https://www.chameleoncg.com/#careers