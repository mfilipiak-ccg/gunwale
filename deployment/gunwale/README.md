# Gunwale Dependency Chart

This chart is intended to house any dependencies for the cluster.

Since most of the dependencies we are using right now really just relied on pulling Git repos, we moved to managing them as onboard dependencies. If there are additional dependencies that are better managed as straight helm dependencies, then they would go here.

Longhorn & Istio are the current dependencies. We pull the repositories inside the onboard child repo section. The alternative is to download the repo, and tar up the helm chart. Then the tar can be placed under the `charts/` directory. The issue was doing so required additional scripts when we wanted to update. This process may be easier with future version of helm and/or the dependencies themselves. Managing them with onboard allowed us to leverage existing support, rather than awkward `update_dependencies.sh` type scripts.