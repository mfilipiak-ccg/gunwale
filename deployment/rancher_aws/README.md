# Rancher Deploy

Uses terraform to provision the server and worker nodes inside of AWS.

It is heavily based on Ranchers quickstart guide. It has been adjusted to auto mount a larger block device, and add an external insecure registry for loading external images. 

https://rancher.com/docs/rancher/v2.x/en/quick-start-guide/deployment/amazon-aws-qs/

## Configuration 

See `terraform.tfvars.sample` for configuration options available. Recommend configuring the following to your liking.

```sh
> cp terraform.tfvars.sample terraform.tfvars
```

* aws_access_key & aws_secret_key must be provided
* ssh_key_name: The AWS keypair to use for your instances
* region: The region for your resources
* type: The size of your worker nodes

TODO The following are not currently exposed to the tfvars.

* connection/private_key for rancheragent-all ssh connection
* rancheragent-all provisioner insecure-registry host/port

## Deploying

```sh
> ./terraform apply -auto-approve
```

## Connecting kubectl

After launching, Rancher will spit out a URL (default login is admin/admin).

Navigate to the quickstart cluster -> Press the Kubeconfig File blue button near the top right of the screen. Copy the file to ~/.kube/config, or set the KUBECONFIG to the file location in your shell.

## Dependencies

* An external docker registry
