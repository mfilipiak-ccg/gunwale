#
# Variables Configuration
#

variable "cluster-name" {
  default = "gunwale-eks"
  type    = "string"
}
