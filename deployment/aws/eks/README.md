# EKS

Using these guides as the baseline for deploying EKS.

https://learn.hashicorp.com/terraform/aws/eks-intro   
https://github.com/terraform-providers/terraform-provider-aws

## Create cluster

```
> ./terraform apply .
```

## Connect kubectl to EKS

The terraform file actually will spit out a kubectl config at the end of the configuration. These steps will walk through a more generic approach, so we know how to connect to an existing EKS cluster as well.

If aws CLI isn't already installed, use pip to install it.

```
> pip install awscli
```

Connect to the cluster with the aws command

```
aws eks --region us-east-1 update-kubeconfig --name gunwale-eks
```

## Connect instances/nodes to the cluster

If you look at the nodes added to the cluster, at this point, there likely won't be any yet.

```
> kubectl get nodes                                                                               
No resources found.
```

You need to push an IAM role to the instance for it to join the cluster. This is mostly wired in from terraform, but needs one additional step.

```
> ./terraform output config_map_aws_auth | kubectl apply -f -
> kubectl get nodes --watch
```