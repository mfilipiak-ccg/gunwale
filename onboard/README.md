# Onboard

Building a multiple project CI/CD system can be extremely difficult considering the various ways in which development and deployment occur for various projects. When viewing DevSecOps at the macro level for each product, there is a certain level where establishing a delivery convention can ease the pain of implementating certain aspects of a delivery pipeline.

In fact, if you follow common patterns for your project, the entire burden of CI/CD can be provided for you. Onboard is focused on tackling the delivery and DevSecOps at a macro levels for disperse projects that will deploy to a shared environment.

See the generated onboard utility: [Onboard Rustbook](../rustbook/onboard/index.html)

## Overview

The onboard utility is a method of ingesting new iterations of software into the appropriate repositories. The goal is to provide a means for a common CM for all artifacts entering the pipeline. This utility is not part of the pipeline specifically, but serves as the means for making artifacts available in the pipeline.

* Exported docker images into a docker capable repository
* Exported Git repositories into a master integration repository
* Helm charts
* More to come...

## Dependencies

* Git Repository
* Docker registry
* Chart Museum for helm chart deployment

## Reference/Examples

The reference project (the project that is ingested into the system) is found here: https://gitlab.com/mfilipiak-ccg/mudpit

### Generating a package

To ease the creation of a compliant package, the tool has the ability to generate one. This is the recommended way to construct a package. It is possible to create a package without the tool, and that is described below in the [Delivery Package](#delivery-package) section.

Use the `onboard create {onboard}.yaml` option to create a package

The manifest is a yaml file describing the targets to package up. The tool will do the work to place them in the correct places and create a compatible delivery manifest.

```yaml
# required description of the project
name: mudpit
version: 0.1.1

# Source code repo to include
# Git is the only one currently supported
# Note: LFS is currently untested
repo:
  - repo_type: git
    repo: git@gitlab.com:mfilipiak-ccg/mudpit.git
    branch: master
    blacklisted_source:
      - tests/
      - scripts/
      - deps/
    tests:
      - tests
    helm:
      - helm/mudpit

child_repos:
  - repo_type: git
    repo: git@gitlab.com:mfilipiak-ccg/water.git
    branch: release
    truncate_history: true

# A list of docker containers to include
containers:
  - name: mudpit:0.1
    dockerfile: Dockerfile
```

It is not necessary to declare the images used in the helm charts separately in the docker section. `onboard` will package them for you. Note that the images discovered via helm will have the autopull & allowPullFail options set to `true`. If that is not desired, you can override any of those image settings by setting the parameters appropriately in the `containers` section manually.

Currently, the tool will output the data in `.onboard/package`. That directory can then be delivered to the target pipeline system and ingested. Future iterations will include cyrpto and compression for the package.

### Delivery Package

To ingest into the pipeline repositories, point the onboard tool at a valid package. It will parse the manifest and track metadata about the artifacts in a MongoDB instance.

You will use a configuration file to ingest all packages that points to the services required for this tool.

```
docker_registry: registry.lab.com:5000
git_host: gitlab.lab.com
gitlab_token: aK3Yj99001nP5aaAAzJ4
```

Then execute the command to ingest the package into the repositories

```
> onboard load -c ~/onboard_config.yaml .onboard/package
```

Here is the reference load package created by the onboard tool, it may be constructed manually for unique cases that require it.

``` yaml
---
name: mudpit
version: 0.1.1
containers:
  # This was generated by the onboard tool to prevent collisions
  # This is the tar file that may contain several docker images (a result of docker export)
  load_files:
    - dbcdb39f0bcf40fd9e9e8507bc547aff.tar
  container_names:
    - "mudpit:0.1"
git:
  - repo: mudpit
    project: mudpit
    branch: master
helm_charts:
  - mudpit-0.1.1.tgz
```

## Usage/Command Line Options

In general, options are provided via a configuration file, to encourage consistency throughout the environment. The latest list of available command line options and usage instructions is available through via the help command.

```
> onboard --help
```

#### Mongo Introspection

```sh
> docker exec -it mongo mongo
mongo> use onboard;
mongo> show collections;
mongo> db.onboard_release.find()
```

#### Building the application

The quickest way is to build from inside the rust container.

```sh
gunwale/onboard> make drop-to-docker
/src> cargo build
/src> cargo test

# run
gunwale/onboard> target/debug/onboard --help
```

## Graveyard

* We used to package up helm charts into a tgz and push them to a helm repository
  * It seems that simply referencing the charts in the git repositories is easier to manage. The chart repo doesn't seem to add much for us