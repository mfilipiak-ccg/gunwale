use regex::Regex;
use std::io::{self, Write};

use crate::repository::ArtifactRepository;
use crate::repository::docker::ContainerRepo;

lazy_static! {
    static ref IMG_RE: Regex = Regex::new(r#"^(\s*-?\s*image:\s+)"?([^"]+)"?$"#).unwrap();
}

pub fn normalize_from_stdin<AR: ArtifactRepository>(repo: &AR, registry_name: &str, simple_match: bool) {
    loop {
        let mut buf = String::new();
        match io::stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_bytes_read) => {
                if simple_match { 
                    let normalized_name = repo.container_repo().normalize_name(buf.trim_end().to_owned(), Some(registry_name));
                    println!("{}", normalized_name);
                } else if let Some(captures) = IMG_RE.captures(buf.trim_end()) {
                    if let Some(img_name) = captures.get(2) {
                        let normalized_name = repo.container_repo().normalize_name(img_name.as_str().to_owned(), Some(registry_name));
                        if let Some(prefix) = captures.get(1) {
                            println!("{}\"{}\"", prefix.as_str(), normalized_name);
                        }
                    }
                } else {
                    io::stdout().write_all(buf.as_bytes()).expect("Could not write to stdout");
                }
            },
            Err(e) => panic!("Error reading stdin: {}", e)
        }
    }
}