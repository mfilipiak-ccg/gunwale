//! An interface representing the target repositories used for onboard
pub mod git;
pub mod docker;
pub mod skopeo;

use docker::{ContainerRepo, DockerRepo};
//use skopeo::{SkopeoRepo};
use git::{GitRepo, Gitlab};
use crate::config::Config;

pub trait ArtifactRepository {
    type CR: ContainerRepo;
    type GR: GitRepo;

    fn config(&self) -> &Config;
    fn container_repo(&self) -> &Self::CR;
    fn git_repo(&self) -> &Self::GR;
}

pub struct GitlabDockerRepository {
    config: Config,
    gitlab: Gitlab
}

impl GitlabDockerRepository {
    pub fn new(conf: Option<Config>) -> Self {
        let active_config = conf.unwrap_or_default();
        GitlabDockerRepository {
            config: active_config,
            gitlab: Gitlab
        }
    }
}

impl<'a> ArtifactRepository for GitlabDockerRepository {
    type CR = DockerRepo;
    //type CR = SkopeoRepo;
    type GR = Gitlab;

    fn config(&self) -> &Config {
        &self.config
    }

    fn container_repo(&self) -> &Self::CR {
        &DockerRepo
    }

    fn git_repo(&self) -> &Gitlab {
        &self.gitlab
    }
}