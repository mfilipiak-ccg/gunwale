//! The main entry point for ingesting a package. 
//! 
//! The package is required to have a manifest.yaml and
//! all reference artifacts inside the directory.
//! Consider using `onboard -c {defintion_file}` to create
//! this package
use std::path::{Path};

use crate::repository::ArtifactRepository;
use crate::tagging;

// these are needed to use trait methods, aliased to avoid collisions
use crate::repository::git::GitRepo as GR;
use crate::repository::docker::ContainerRepo as CR;
use crate::description_defs::{Manifest, ManifestGitRepo};


/// Takes a new package and imports it into the system. All resources defined
/// in the manifest will be directed to the appropriate repositories and tracked.
pub fn ingest_package<AR: ArtifactRepository>(filename: &Path, repo: &AR, use_tagging: bool) {
    assert!(filename.exists(), "Could not find artifact file/folder");

    // TODO deal with files and signatures
    if filename.is_dir() {
        ingest_artifact_directory(filename, repo, use_tagging);
    } else {
        panic!("Filename is not a directory, cannot support files at this point")
    }
}

/// Works on ingesting the extracted directory
fn ingest_artifact_directory<AR: ArtifactRepository>(directory_path: &Path, repo: &AR, use_tagging: bool) {
    let manifest_path = directory_path.join(Path::new("manifest.yaml"));
    let artifact_manifest = parse_manifest(&manifest_path);

    // TODO if it fails/panics, we should track it, and then allow recreate
    let package_rev_id = if !use_tagging {None} else {
        println!("Connecting to tagging database...");
        let conn = tagging::create_connection(repo.config());
        Some(tagging::create_revision(&artifact_manifest, conn))
    };
    if let Some(package_id) = &package_rev_id {
        println!("Created package id: {}", package_id);
    }

    //TODO ensure some namespacing happens, so we know we are importing these items exclusively
    // (e.g. we know another package of the same name/version isn't stomping on us at this point)
    if let Some(containers) = artifact_manifest.containers {
        for load_file in containers.load_files.unwrap_or_default() {
            repo.container_repo().load_image(directory_path.join(load_file).as_path());
        }
        ingest_docker_containers(containers.container_names, repo, &package_rev_id);
    }

    if let Some(repos) = artifact_manifest.git {
        ingest_git_source(&package_rev_id, directory_path, repos, repo);
    }
}

fn ingest_docker_containers<AR: ArtifactRepository>(containers: Vec<String>, repo: &AR, package_id: &Option<String>) {
    for container in containers {
        //TODO Refuse to upload the same docker image twice. Worried someone can 
        // invalidate a current install by making the containers mutable
        repo.container_repo().push_image(&container, repo.config().docker_host().as_ref());
        if let Some(package_id) = package_id {
            let conn = tagging::create_connection(repo.config());
            tagging::create_container_push(container.as_ref(), package_id, conn);
        }
    }
}

fn ingest_git_source<AR: ArtifactRepository>(
    package_id: &Option<String>,
    base_directory: &Path,
    git_repos: Vec<ManifestGitRepo>,
    repo: &AR,
) {
    for git_repo in git_repos {
        // Make sure the directory specified in the manfest (that is our git source)
        // exists before attempting to push it
        let repo_path = base_directory.join(git_repo.repo.as_path());
        assert!(
            repo_path.exists(),
            format!("Git repository '{:?}' not found in package", repo_path)
        );

        let git = repo.git_repo();

        // Now make sure the target repository project exists in the ingest environment
        // If this were a new project being added, there is likely no project in git to house it
        if !git.check_project_exists(&git_repo.project, repo.config()) {
            println!("Project '{}' does not exist in GitLab, creating it...", git_repo.project);
            if let Err(e) = git.create_project(&git_repo.project, repo.config()) {
                panic!("Error creating project in gitlab: {}", e);
            }
        }

        // TODO we should have push_rep return a result and pause and retry a few times if it failed
        //println!("Sleeping 15 seconds to ensure project is ready to accept a push...");
        //thread::sleep(time::Duration::from_secs(15));

        // Push the repository to the remote target repo
        let sha = git.push_repository(repo_path.as_path(), &git_repo.project, &git_repo.branch, &repo.config());
        if let Some(pkg_id) = package_id {
            let conn = tagging::create_connection(repo.config());
            tagging::create_git_push(&sha, pkg_id.as_ref(), &git_repo.project, &git_repo.branch, conn);
        }

        git.update_submodule(&git_repo.project, &sha, &repo.config());
    }
}


fn parse_manifest(filename: &Path) -> Manifest {
    assert!(filename.exists(), "Manifest does not exist");

    let f = std::fs::File::open(filename).expect("Cannot open file");
    let model: Manifest = serde_yaml::from_reader(f).expect("Cannot parse yaml");
    model
}