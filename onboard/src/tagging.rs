//! A tagging database for CM purposes.
//!
//! We use our own database to manage tracking artifacts. This lets the logic
//! of the tool be more agnostic of the backing storage. Some features vary
//! significantly in terms of tagging support, and some tools require you to
//! abandon the free tier to get support for it.
use mongodb::coll::options::{IndexOptions, FindOptions};
use mongodb::db::ThreadedDatabase;
use mongodb::{bson, doc, Bson};
use mongodb::Bson::{ObjectId};
use mongodb::{Client, ThreadedClient};
use std::io::Error;
use std::path::PathBuf;

use crate::description_defs::{Manifest, ManifestGitRepo};
use crate::config::Config;
use chrono::Utc;

// XXX figure out how to do pooling
pub fn create_connection(config: &Config) -> Client {
    let mongo_uri = config.mongo_uri.clone().unwrap_or_default();
    let mut uri_parts = mongo_uri.split(':');

    Client::connect(
        uri_parts.next().unwrap_or("pipeline-mongodb.pipeline"),
        uri_parts.next().unwrap_or("27017").parse().expect("Invalid port specified for MongoDB connection")
    ).unwrap()
}

/// Create a new revision for a package, and store it in the database
pub fn create_revision(manifest: &Manifest, client: Client) -> String {
    let onboard_db = client.db("onboard");

    let coll = onboard_db.collection("onboard_release");
    // XXX probably a better place to create this index,
    // It doesn't seem to create any issue to recreate it
    coll.create_index(
        doc! {"name": 1, "version": 1},
        Some(IndexOptions {
            unique: Some(true),
            ..Default::default()
        }),
    )
    .expect("Could not create index for onboard_release collection");

    let manifest_bson = bson::to_bson(&manifest).expect("Could not convert manifest to BSON");
    if let bson::Bson::Document(mut doc) = manifest_bson {
        doc.extend(doc! {"datetime_utc": Utc::now()});

        // if there is an inserted_id (and its of type String, for convience one-liner),
        // return it for referencing elsewhere when inserting the pieces
        // If it is absent, the release is already imported and we should force a version bump
        if let Some(ObjectId(id)) = coll
            .insert_one(doc, None)
            .expect("Failed to insert document")
            .inserted_id
        {
            id.to_hex()
        } else {
            // XXX provide reference to the existing item for removal or forcing an update
            panic!("Release already exists, or is being created");
        }
    } else {
        panic!("Cannot convert struct to BSON");
    }
}

/// Track a git push inside a onboard.git_pushes collection
///
/// We track the {package_id, project, branch, sha} as a reference
/// for this package
pub fn create_git_push(
    sha: &str,
    package_id: &str,
    project: &str,
    branch: &str,
    client: Client,
) {
    let onboard_db = client.db("onboard");

    let coll = onboard_db.collection("git_pushes");

    let git_bson = doc! {
        "package_id": package_id,
        "project": project,
        "branch": branch,
        "sha:": sha
    };

    coll.insert_one(git_bson, None)
        .expect("Failed to insert document");
}

/// Track a docker container push inside a onboard.containers collection
///
/// We track the {package_id, name}
pub fn create_container_push(name: &str, package_id: &str, client: Client) {
    let onboard_db = client.db("onboard");
    let coll = onboard_db.collection("containers");

    let container_bson = doc! {
        "container_name": name,
        "package_id": package_id,
    };

    coll.insert_one(container_bson, None)
        .expect("Failed to insert document");
}

pub fn query_docker_containers(
    package_id: &str,
    client: Client,
) -> Result<Vec<String>, Error> {
    let onboard_db = client.db("onboard");
    let coll = onboard_db.collection("containers");

    let query_bson = doc! {
        "package_id": package_id,
    };
    let cursor = coll.find(Some(query_bson), None)?;
    let containers: Vec<String> = cursor
        .map(|result| {
            if let Ok(item) = result {
                if let Some(&Bson::String(ref name)) = item.get("container_name") {
                    return Some(name.to_owned());
                }
            }
            None
        })
        .filter(Option::is_some)
        .map(|a| a.unwrap())
        .collect();
    Ok(containers)
}

pub fn query_helm_charts( package_id: &str, client: Client) -> Result<Vec<HelmQueryResult>, Error> {
    let onboard_db = client.db("onboard");
    let coll = onboard_db.collection("onboard_release");

    let query_bson = doc! {
        "_id": bson::oid::ObjectId::with_string(package_id).unwrap(),
    };
    let projection = doc! {
        "git": 1,
        "name": 1,
    };
    let find_options = FindOptions {projection: Some(projection), ..Default::default()};
    let cursor = coll.find(Some(query_bson), Some(find_options))?;
    let charts: Vec<HelmQueryResult> = cursor
        .map(|result| {
            if let Ok(item) = result {
                // XXX there is some aggregate to flatten these in mongo certainly
                if let Some(&Bson::String(ref name)) = item.get("name") {
                    let repos: Vec<ManifestGitRepo> = bson::from_bson(item.get("git")
                        .unwrap().to_owned())
                        .expect("Could not extract object from document");
                    let chart: Vec<HelmQueryResult> = repos.iter()
                        .flat_map(|r| {
                            r.helm.iter().map(|h| {
                                HelmQueryResult {
                                    proj_name: name.to_string(),
                                    chart_location: h.chart.clone(),
                                    values: h.values.clone()
                                }
                            })
                        })
                        .collect();
                    
                    return Some(chart);
                } else {
                    panic!("No name found for onboard release and id: {}", package_id);
                };
            };
            None
        })
        .filter(Option::is_some)
        .map(|a| a.unwrap())
        .flatten()
        .collect();
    Ok(charts)
}

pub fn query_repos(
    package_id: &str,
    client: Client,
) -> Result<Vec<ManifestGitRepo>, Error> {
    let onboard_db = client.db("onboard");
    let coll = onboard_db.collection("onboard_release");

    let query_bson = doc! {
        "_id": bson::oid::ObjectId::with_string(package_id).unwrap(),
    };
    let projection = doc! {
        "git": 1,
    };
    let find_options = FindOptions {projection: Some(projection), ..Default::default()};
    let cursor = coll.find(Some(query_bson), Some(find_options))?;
    let tests: Vec<ManifestGitRepo> = cursor
        .map(|result| {
            if let Ok(item) = result {
                let repo: Vec<ManifestGitRepo> = bson::from_bson(item.get("git").unwrap().to_owned())
                    .expect("Could not extract object from document");
                return Some(repo);
            }
            None
        })
        .filter(Option::is_some)
        .map(|a| a.unwrap())
        .flatten()
        .collect();
    Ok(tests)
}

pub struct HelmQueryResult {
    pub proj_name: String,
    pub chart_location: PathBuf,
    pub values: Option<Vec<PathBuf>>
}
