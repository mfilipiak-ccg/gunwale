#![macro_use]

/// Helper to clean up running commands, especially a series of commands. It will take a
/// directory to execute from, a comment about the command it is running (for debugging and
/// error messages), and the command to run.
/// 
/// It will panic on a failed command, and opionally parse stdout to a string (if assigned to a
/// variable)
///
/// ```
/// let two_arg_list: Vec<&str> = create_echo2_arg_vec();
/// run_shell { &path_of_directory =>
///     "Running the first echo" =>
///         "echo", 1;
///     "Running the second echo w/ arg Vec, and assigning stdout to a variable" =>
///         let stdout_str = "echo" [two_arg_list];
///     "Running third echo to another variable" =>
///         let stdout_3cmd_str = "echo", "store this"
/// };
/// 
/// println!("{} and then {}", stdout_str, stdout_3cmd_str);
/// ``` 
macro_rules! run_shell {
    // This is the variation with comma separated args
    // 
    // ```
    // run_shell! { dir =>
    //     "Message" => 
    //         "echo", "boomerang"
    // }
    // ```
    ($dir:expr => $cmt:expr => $cmd:literal, $($arg:expr),*) => {
        {
            let mut args: Vec<String> = Vec::new();
            $( args.push($arg.to_string()); )*
            run_shell! {$dir => $cmt => $cmd [args]}
        }
    };
    // This is the variation with an array list for args
    // 
    // ```
    // run_shell! { dir =>
    //     "Message" => 
    //         "echo", "boomerang"
    // }
    // ```
    ($dir:expr => $cmt:expr => $cmd:literal [$args:expr]) => {
        {
            println!("{}", $cmt);
            //println!("{} {:?}", $cmd, $args);
            let cmd = Command::new($cmd)
                .args(&$args)
                .current_dir($dir)
                .output()
                .unwrap_or_else(|_| panic!("Failed: {}", $cmt));

            // make sure the process ran correctly
            if !cmd.status.success() {
                // output the error if it didn't
                io::stdout().write_all(&cmd.stdout).unwrap();
                io::stderr().write_all(&cmd.stderr).unwrap();
                panic!("Failed: {}", $cmt);
            }
        }
    };
    // This is the variation with comma separated args and assigned to a variable
    // 
    // ```
    // run_shell! { dir =>
    //     "Message" => 
    //         let rst = "echo", "boomerang"
    // }
    // ```
    ($dir:expr => $cmt:expr => let $v:ident = $cmd:literal, $($arg:expr),*) => {
        let temp_val;
        {
            let mut args: Vec<String> = Vec::new();
            $( args.push($arg.to_string()); )*
            run_shell! {$dir => $cmt => let inside_temp = $cmd [args]};
            temp_val = inside_temp;
        }

        let $v = temp_val;
    };
    // This is the variation with an array list for args, and assigned to a variable
    // 
    // ```
    // run_shell! { dir =>
    //     "Message" => 
    //         let rst = "echo" [arg_list_slice]
    // }
    // ```
    ($dir:expr => $cmt:expr => let $v:ident = $cmd:literal [$args:expr]) => {
        let temp_val;
        {
            println!("{}", $cmt);
            //println!("{} {:?}", $cmd, $args);
            let cmd = Command::new($cmd)
                .args(&$args)
                .current_dir($dir)
                .output()
                .unwrap_or_else(|_| panic!("Failed: {}", $cmt));
            let stdout = cmd.stdout;

            // make sure the process ran correctly
            if !cmd.status.success() {
                // output the error if it didn't
                io::stdout().write_all(&stdout).unwrap();
                io::stderr().write_all(&cmd.stderr).unwrap();
                panic!("Failed: {}", $cmt);
            }
            temp_val = String::from_utf8(stdout)
                .expect("Could not output from stdout")
                .trim_end()
                .to_owned();
        }
        let $v = temp_val;
    };
    // handle multiple commands in a single block
    ($dir:expr => $cmt1:expr => $c1:literal, $($a1:expr),*; $($rest:tt)+) => {
        run_shell! {$dir => $cmt1 => $c1, $($a1),*}
        run_shell! {$dir => $($rest)*}
    };
    // handle the let assignment case inside a multiple definition
    ($dir:expr => $cmt1:expr => let $v:ident = $c1:literal, $($a1:expr),*; $($rest:tt)+) => {
        run_shell! {$dir => $cmt1 => let $v = $c1, $($a1),*}
        run_shell! {$dir => $($rest)*}
    };
    // handle the let assignment case inside a multiple definition (array variation)
    ($dir:expr => $cmt1:expr => $c1:literal, [$a1:expr]; $($rest:tt)+) => {
        run_shell! {$dir => $cmt1 => $c1, [$a1]}
        run_shell! {$dir => $($rest)*}
    };
    // handle the let assignment case inside a multiple definition (assign and array variation)
    ($dir:expr => $cmt1:expr => let $v:ident = $c1:literal, [$a1:expr]; $($rest:tt)+) => {
        run_shell! {$dir => $cmt1 => let $v = $c1, [$a1]}
        run_shell! {$dir => $($rest)*}
    };
}
