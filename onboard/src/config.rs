//! Onboard load uses a config file to describe the target environment
//! where artifacts are deployed into. These are unlikely to change over time,
//! so we supply a consistent file instead of flags for these configurations.
//! 
//! ```
//! docker_registry: docker.registry.lab:5000
//! git_host: gitlab.lab
//! gitlab_token: thisISMY_TokenFromGitlab_1y
//! git_master_project: master_CM_Project
//! ```
use std::fs::{File};
use std::path::{Path};

/// This is the config definition for the onboard load config file
#[derive(Debug, Deserialize, Serialize, Default, Clone)]
pub struct Config {
    /// The host/port for the docker registry to push images to. (e.g. docker.registry.lab:5000)
    pub docker_registry: Option<String>,

    /// (Deprecated) If using tagging, this is the URI for the mongoDB server
    pub mongo_uri: Option<String>,

    /// The GitLab hostname
    pub git_host: Option<String>,

    /// The git SSH host. This is useful in rare situations where the 
    /// SSH host is non-standard. It will default to git_host if
    /// not defined
    pub git_ssh_host: Option<String>,

    /// The name of the project that houses submodules relating to
    /// this project. We can update the submodule automatically, and
    /// create a merge request with this project reference
    pub git_master_project: Option<String>,

    /// The API token from GitLab that enabled us to speak to the API
    pub gitlab_token: Option<String>,

    /// Either http or https. Defaults to https
    #[serde(default = "default_https")]
    pub gitlab_proto: String,

    /// The gitlab user where all projects will be placed.
    /// Defaults to 'pipeline'
    #[serde(default = "default_pipeline")]
    pub gitlab_user: String
}

fn default_https() -> String { "https".to_string() }
fn default_pipeline() -> String { "pipeline".to_string() }

impl Config {

    pub fn docker_host(&self) -> String {
        // lazily errors out, which allows us to only require features we use
        self.docker_registry
            .as_ref()
            .expect("Need to specify a docker registry host via the configuration file")
            .clone()
    }
}

/// This method will load the given configuration file and return the parsed YAML object
pub fn load_config_file<P: AsRef<Path>>(config_file: P) -> Config {
    let f = File::open(config_file).expect("Cannot open configuration file");
    let config: Config = serde_yaml::from_reader(f).expect("Cannot parse configuration file yaml");
    config
}