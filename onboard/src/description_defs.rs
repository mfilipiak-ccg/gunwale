//! Definitions for various yaml files used by this tool.
//! 
//! [`CreationDef`](struct.CreationDef.html)
//! : The yaml file that should be inside the repo to describe the items in the repo for the tool.
//! This is primarily used by `onboard create` and the CI pipeline to understand the repo
//! 
//! [`Manifest`](struct.Manifest.html)
//! : This is the calculated description file for the repository, most likely a result of 
//! `onboard create`. Although, it may be created manually to construct a package without using the
//! create capability.
 
use std::path::{PathBuf};
use std::collections::{BTreeMap};
 
/// The definition that describes the resources that you want packaged
/// up for delivery. It is intential that create attempts to force that
/// commits are pushed to a remote before packaging. It is important that each
/// package created is tracable back to the ref (strongly discouraging one time builds
/// from a developer checkout, or otherwise mutated source).
/// [`CreationDef`](struct ::CreationDef)
/// 
/// ```
/// # required description of the project
/// name: mudpit
/// version: 0.1.1
/// 
/// # Source code repo to include
/// # Git is the only one currently supported
/// # Note: LFS is currently untested
/// repo:
///   - repo_type: git
///     repo: git@gitlab.com:mfilipiak-ccg/mudpit.git
///     branch: master
///     blacklisted_source:
///       - tests/
///       - scripts/
///       - deps/
///     tests:
///       - .gitlab-ci-integration.yaml
///     helm:
///       - helm/mudpit
/// 
/// child_repos:
///   - repo_type: git
///     repo: git@gitlab.com:mfilipiak-ccg/water.git
///     branch: release
///     truncate_history: true
/// 
/// # A list of docker containers to include
/// containers:
///   - name: mudpit:0.1
///     dockerfile: Dockerfile
/// 
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CreationDef {
    /// The name of the delivery package
    pub name: String,

    /// The version of the delivery package
    pub version: String,

    /// A list of docker containers
    #[serde(default = "Vec::new")]
    pub containers: Vec<Containers>,
    
    /// The details for this repository. This is
    /// required. We will pull down a fresh copy
    /// to avoid including untracked files 
    /// (e.g. a developer's working directory)
    pub repo: GitRepo,

    /// A list of child repositories to include
    /// to support this deployment
    #[serde(default = "Vec::new")]
    pub child_repos: Vec<GitRepo>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct GitRepo {
    /// For now, only 'git' is supported
    pub repo_type: String,

    /// The full URL to the git repo.
    ///
    /// You may use @this to autodiscover the repo from the
    /// current repository
    pub repo: String,

    /// The branch to package up
    ///
    /// You may use @this to autodiscover the current  branch
    /// from the current repository
    pub branch: String,

    /// When true, use --depth 1 during checkout. Note
    /// this will cause history to be truncated. Useful for
    /// third party repositories where size and speed may be
    /// worth the sacrifice of the Git history.
    /// 
    /// Default: false
    #[serde(default)]
    pub truncate_history: bool,

    /// A list of globs (relative to the repository) that should
    /// be excluded from static analysis.
    /// 
    /// We prefer blacklisting (which is contrary to our general model)
    /// because it is more reasonable to audit things excluded rather than
    /// ensuring everything is 'included'
    #[serde(default = "Vec::new")]
    pub blacklisted_source: Vec<String>,

    /// A list of helm charts to include
    pub helm: Vec<HelmOptions>,

    /// A list gitlab compatible test defintions to run after installation
    #[serde(default = "Vec::new")]
    pub tests: Vec<PathBuf>,
}


#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Containers {
    /// The full container name (including version)
    pub name: String,
    // Build from dockerfile. Otherwise, we assume `docker pull`
    // will be able to resolve and grab the image
    pub dockerfile: Option<String>,
    /// If true and autopull=true, a failure is not considered an error
    #[serde(default)]
    pub allow_pull_failure: bool
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct SourceCode {
    pub directory: PathBuf,
    pub source_type: String
}

/// Options for the helm chart deployment
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct HelmOptions {
    /// The directory the chart is located in, relative to the repository
    pub chart: PathBuf,

    /// A list of optional values files defining how the chart will be deployed
    pub values: Option<Vec<PathBuf>>,

    /// This allows forwarding environment variables into values
    /// inside the chart. An entry: `IS_DEBUG_RELEASE: debug`
    /// would set the value of the IS_DEBUG_RELEASE environment
    /// variable to the equivalent in `helm install --set debug=value`
    #[serde(default)]
    pub forward_vars: BTreeMap<String, String>,

    /// Occasionally charts charts/ directory will break if we repull the dependencies
    /// via `helm dep up` (likely as a result of a broken helm chart)
    /// This option is not typically encouraged. 
    #[serde(default)]
    pub skip_dependency_update: bool,

    /// If true, the namespace should NOT be labeled for automatic sidecar injection
    #[serde(default)]
    pub disable_sidecar_injection: bool
}


#[derive(Debug, Deserialize, Serialize)]
pub struct Manifest {
    pub name: String,
    pub version: String,
    pub containers: Option<DockerContainers>,
    pub git: Option<Vec<ManifestGitRepo>>,
    // XXX releaseNotes
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct ManifestGitRepo {
    pub repo: PathBuf,
    pub project: String,
    pub origin_url: String,
    pub branch: String,
    pub tests: Vec<PathBuf>,
    pub blacklisted_source: Vec<String>,
    pub helm: Vec<HelmOptions>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DockerContainers {
    pub load_files: Option<Vec<PathBuf>>,
    pub container_names: Vec<String>,
}
