#[derive(Debug, Deserialize)]
pub struct Component {
    id: String,
    repository: String,
    format: String,
    name: String,
    version: String,
}

#[derive(Debug, Deserialize)]
pub struct Components {
    items: Vec<Component>,
    //#[serde(rename = "continuationToken")]
    //continuation_token: Option<String>,
}

pub fn _component_list(repository_name: &str) -> Components {
    let client = reqwest::Client::new();
    let url: String = format!(
        "http://localhost:8081/service/rest/v1/components?repository={}",
        repository_name
    );
    let mut response = client
        .get(&url.to_owned())
        //.basic_auth("admin", Some("password"))
        .send()
        .expect("Failed to send request");

    let parsed = response.json::<Components>();
    match parsed {
        Ok(r) => r,
        Err(e) => panic!("Error retrieving the component list: {:?}", e),
    }
}
