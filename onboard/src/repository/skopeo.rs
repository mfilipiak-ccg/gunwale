use super::docker::{ContainerRepo, normalize_img_name};
use crate::description_defs::Containers;
use crate::path_extensions::PathExtensions;

use std::collections::{HashMap, HashSet};
use std::fs::{File};
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::process::{Command};
use uuid::Uuid;

pub struct SkopeoRepo;

impl ContainerRepo for SkopeoRepo {
    fn push_image(&self, _image_name: &str, _repo: &str) {
    }


    fn package_containers(&self, containers: &[&Containers], output_directory: &Path) -> Option<String> {
        // TODO refactor (rip off of docker version for POC)
        if containers.is_empty() {
            println!("No docker images defined, skipping save");
            return None ;
        }
        let save_name = format!("{}.tar", Uuid::new_v4().to_simple().to_string());
        let mut sync_yaml = SkopeoSync::new();
        for container in containers {
            // Build using the dockerfile if one is specified, otherwise, we will
            // assume we can pull the image
            if let Some(dockerfile) = &container.dockerfile {
                println!("Building docker image: {}", container.name);

                // If the dockerfile specification starts with %project, we handle
                // the %project as a reference to a repository. That means, we
                // will look inside of our .onboard/package staging directory for the path.
                // Otherwise, we treat it as a relative path to the current project
                let dockerfile_transformed = if dockerfile.starts_with('%') {
                    output_directory.join(dockerfile.replace('%', ""))
                } else {
                    PathBuf::from(dockerfile).absolute_path()
                };

                // The context for the docker build is the parent directory for the dockerfile
                let basedir = dockerfile_transformed.parent();
                let basedir_str = basedir
                    .expect("No basedir found for the dockerfile")
                    .to_string_lossy();
                let dockerfile = &*dockerfile_transformed.to_string_lossy();
                
                // Execute the docker build with the specified container name, and dockerfile/context
                run_shell! { "." => 
                    "Building docker image" =>
                        "docker", "build", "-t", &container.name, "-f", dockerfile, &*basedir_str
                }
            }

            // Create the skopeo yaml entry that can be used in `skopeo sync` command
            let (registry, img_name, version) = split_img_name(&container.name);
            match sync_yaml.get_mut(registry) {
                // there is an existing registry that matches this one,
                // append the img/version to it
                Some(reg_entry) => {
                    match reg_entry.images.get_mut(img_name) {
                        // the img already has an entry, 
                        Some(img_entry) => {img_entry.insert(version.to_owned());},
                        None => {
                            // there was no img with this name inside
                            // of the registry entry, create one with it's version
                            let mut version_set = HashSet::new();
                            version_set.insert(version.to_owned());
                            reg_entry.images.insert(img_name.to_owned(), version_set);
                        }
                    }
                },
                None => {
                    // the registry didn't exist yet, create the whole structure
                    let mut img_map = HashMap::new();
                    let mut version_set = HashSet::new();
                    version_set.insert(version.to_owned());
                    img_map.insert(img_name.to_owned(), version_set);
                    let entry = SkopeoSyncEntry{images: img_map};
                    sync_yaml.insert(registry.to_owned(), entry);
                }
            }
        }

        let sync_yaml_file = output_directory
            .join("skopeo_sync.yaml");
        let file_handle = File::create(&sync_yaml_file)
            .expect("Could not open output yaml file for skopeo sync command");
        serde_yaml::to_writer(file_handle, &sync_yaml)
            .expect("Could not write yaml file for skopeo sync command");

        let sync_yaml_str = sync_yaml_file.to_string_lossy();
        let container_dir = output_directory.join("skopeo_containers");
        let out_dir = container_dir.to_string_lossy();

        run_shell! { output_directory => 
            "Saving docker image" => 
                "skopeo", "sync", "--src", "yaml", "--dest", "dir", sync_yaml_str, out_dir
        }

        // return the saved name
        Some(save_name)
    }

    fn load_image(&self, _filename: &Path) {

    }

    fn normalize_name(&self, name: String, registry_name: Option<&str>) -> String {
        normalize_img_name(name, registry_name)
    }
}

fn split_img_name(name: &str) -> (&str, &str, &str) {
    // XXX this is a third copy of similar logic, refactor
    let slash = name.find('/');
    if let Some(slash_index) = slash {
        let domain = &name[..slash_index];
        // there is a slash, check if it looks like a hostname
        if domain == "localhost" || domain.chars().any(|c| c == '.' || c == ':') {
            let remainder = &name[slash_index + 1..];
            let (img_name, version) = split_img_version_without_domain(remainder);
            // adding 1 removes the /
            return (domain, img_name, version);
        } 
    }
    // assumes the docker.io as the default registry since none was specified
    let (img_name, version) = split_img_version_without_domain(name);
    ("docker.io", img_name, version)
}

/// Given a docker image name without the domain (already extracted),
/// return the image name and version portion
fn split_img_version_without_domain(name: &str) -> (&str, &str) {
    let colon = name.find(':');
    if let Some(colon_index) = colon {
        return (&name[..colon_index], &name[colon_index + 1..]);
    }
    // Default to 'latest' for the version
    (name, "latest")
}

/// registry.example.com:
///     images:
///         busybox: []
///         redis:
///             - "1.0"
///             - "2.0"
///     credentials:
///         username: john
///         password: this is a secret
///     tls-verify: true
///     cert-dir: /home/john/certs
/// quay.io:
///     tls-verify: false
///     images:
///         coreos/etcd:
///             - latest
type SkopeoSync = HashMap<String, SkopeoSyncEntry>;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SkopeoSyncEntry {
    images: HashMap<String, HashSet<String>>
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_skopeo_normalize_name() {
        assert_eq!(("docker.io", "something", "latest"), split_img_name("something:latest"));
        assert_eq!(("repo.test", "something", "latest"), split_img_name("repo.test/something:latest"));
        assert_eq!(("repo:5000", "something", "latest"), split_img_name("repo:5000/something:latest"));
        assert_eq!(("repo.test:5000","something", "latest"), split_img_name("repo.test:5000/something:latest"));
        assert_eq!(("docker.io", "something/else", "latest"), split_img_name("something/else:latest"));
        assert_eq!(("docker.io", "something/else", "latest"), split_img_name("docker.io/something/else:latest"));
        assert_eq!(("repo.test", "something.else/entirely", "latest"), split_img_name("repo.test/something.else/entirely:latest"));
        assert_eq!(("repo.test", "something.else/entirely", "latest"), split_img_name("repo.test/something.else/entirely:latest"));
        // infer the versions
        assert_eq!(("repo.test", "something.else/entirely", "latest"), split_img_name("repo.test/something.else/entirely"));
        assert_eq!(("docker.io", "something", "latest"), split_img_name("something"));
    }
}