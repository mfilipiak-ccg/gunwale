//! Module for interacting with Git
use regex::Regex;
use std::io::{self, Write};
use std::collections::BTreeMap;
use std::path::Path;
use std::process::{Command};
use reqwest::{self, StatusCode};
use std::borrow::Cow;
use tempdir::TempDir;

use crate::config::Config;

pub trait GitRepo {
    /// Checks if a project exists with the given name
    fn check_project_exists(&self, project_name: &str, config: &Config) -> bool;
    fn create_project(&self, project_name: &str, config: &Config) -> Result<(), &'static str>;
    fn package_repo(&self, repo: &str, branch: &str, truncate_history: bool, out_dir: &Path) -> String;
    fn push_repository(&self, repo_path: &Path, project: &str, branch: &str, config: &Config) -> String;
    fn list_submodules(&self, repo_path: &Path) -> Vec<String>;
    fn update_submodule(&self, project_name: &str, sha: &str, config: &Config);
}

pub struct Gitlab;

// Just ignore all keys, we're only interested in the length of items found
#[derive(Deserialize)]
struct GitLabProjectResponse {}

impl GitRepo for Gitlab {

    fn check_project_exists(&self, project_name: &str, config: &Config) -> bool {
        // Build up the REST endpoint location
        // The projects name is the URL-encoded namespace/project_path (%2F is /)
        let url = format!("{}://{}/api/v4/projects?owned=true&simple=true&search={}",
            config.gitlab_proto,
            config.git_host.as_ref().expect("Git host not specified in the config file"), 
            project_name);

        let gitlab_token = config.gitlab_token.as_ref()
            .expect("No git token was specified in the config file");

        // Make the request to the REST server
        let client = reqwest::Client::builder()
            .danger_accept_invalid_certs(true)
            .build()
            .expect("Could not build reqwest client for REST requests to Git");
        let mut resp = client.get(url.as_str())
            .header("PRIVATE-TOKEN", gitlab_token)
            .send()
            .expect("Error querying git to see if the project already exists");

        // Process the result, expecting to get success for creation
        match resp.status() {
            StatusCode::OK => {
                let json: Vec<GitLabProjectResponse> = resp.json()
                    .expect("Invalid project query response");
                !json.is_empty()
            }
            StatusCode::NOT_FOUND => false,
            _ => panic!(format!("Unexpected status code received from git while trying to query \
                        for project existing: {:?}", resp.status()))
        }
    }

    fn create_project(&self, project_name: &str, config: &Config) -> Result<(), &'static str> {
        // Build up the REST endpoint location
        let url = format!("{}://{}/api/v4/projects?access_token={}", 
            config.gitlab_proto,
            config.git_host.as_ref().expect("Git host not specified in the config file"), 
            config.gitlab_token.as_ref().expect("No git token was specified in the config file"));
        // XXX creating this as public right now, because of the way the pipeline clones and 
        // doesn't wire creds. Fixing that will make this capable of being 
        // private again (or make it a config)
        let params = [("name", project_name), ("visibility", "public")];

        // Make the request to the REST server
        let client = reqwest::Client::builder()
            .danger_accept_invalid_certs(true)
            .build()
            .expect("Could not build reqwest client for REST requests to Git");
        let res = client.post(url.as_str())
            .form(&params)
            .send();
        
        // Process the result, expecting to get success for creation
        if let Ok(response) = res {
            match response.status() {
                StatusCode::OK | StatusCode::CREATED => return Ok(()),
                StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN => return Err("Authorization denied while trying to create git project. Check gitlab access token"),
                _ => {}
            } 
        };
        Err("Failed to create the gitlab project")
    }

    /// Pushes a git directory to the integration remote repository
    ///
    /// Returns: The SHA representing the HEAD of the newest commit
    fn push_repository(&self, repo_path: &Path, project: &str, branch: &str, config: &Config) -> String {
        // use git_ssh_host if defined, otherwise, we default to the git_host
        let ssh_host = get_git_ssh_host(config);
        let git_path= format!("git@{}:{}/{}.git", 
            ssh_host,
            config.gitlab_user,
            project
        );

        run_shell! { repo_path => 
            // defensively remove any existing remote here
            // TODO this now needs to allow failures
            //"Removing any existing onboard remote" =>
                //"git", "remote", "remove", "onboard-integration";
            "Copying manifest into the repo" =>
                "cp", "../manifest.yaml", "onboard_manifest.yaml";

            "Adding onboard manifest to repo" =>
                "git", "add", "onboard_manifest.yaml";

            "Commiting changes to the repo" =>
                "git", "add", "onboard_manifest.yaml";

            // create the new remote
            "Adding the remote for onboard repository" => 
                "git", "remote", "add", "onboard-integration", &git_path;

            // now push up to the remote we wired in
            "Pushing package to the remote repository" => 
                "git", "push", "-u", "onboard-integration", &branch;

            // need to pull out the latest revision we just pushed to track it
            "Get latest Git revision ref" =>
                let sha_cmd = "git", "rev-parse", &format!("--branches={}", branch), "--verify", "HEAD"
        }

        sha_cmd
    }

    fn package_repo(&self, repo_config: &str, branch_config: &str, truncate_history: bool, out_dir: &Path) -> String {
        assert!(
            out_dir.exists() && out_dir.is_dir(),
            "Output directory must exist and be a directory"
        );

        let repo = match repo_config {
            "@this" => Cow::from(discover_remote_url(&out_dir)),
            _ => Cow::from(repo_config)
        };

        let branch = match branch_config {
            "@this" => Cow::from(discover_current_branch(&out_dir)),
            _ => Cow::from(branch_config)
        };

        lazy_static! {
            static ref GIT_RE: Regex = Regex::new(r".*/([^\.]+).git$").unwrap();
        }

        // This needs to be a stable/predicatable name so it can be references elsewhere if needed
        // (e.g. if a helm chart lives inside the directory)
        let name = GIT_RE
            .captures(&repo)
            .expect("Repo format is unknown (1)")
            .get(1)
            .expect("Repo format is unknown (2)")
            .as_str();
        
        // Build up the git arguments, optionally adding some based on the clone options supplied
        let mut clone_args = vec!["clone", &repo, "--branch", &branch, "--single-branch", 
            "--recurse-submodules", "-j8"];
        if truncate_history {
            clone_args.append(&mut vec!["--shallow-submodules", "--depth", "1"]);
        }
        // This is the checkout directory name (matches the git repo name)
        clone_args.push(name);

        run_shell! { out_dir =>
            format!("Cloning repository {} with branch {}", repo, branch) =>
                "git" [clone_args]
        }

        String::from(name)
    }

    fn list_submodules(&self, repo_path: &Path) -> Vec<String> {
        run_shell! { repo_path => 
            "List Git submodules" =>
                let submodule_list = "git", "submodule", "status"
        }
        let modules: Vec<String> = submodule_list
            .split('\n')
            // deinitialized submodules come in the format:
            // '-SHA submodule-name'
            // if they are initialized they look like:
            // ' SHA submodule-name (heads/master)'
            // A trim_start normalized the 2 versions
            .filter_map(|s| s.trim_start().split(' ').nth(1))
            .map(|s| s.to_owned())
            .collect();
        modules
    }

    /// Shallow clones the master project into a temporary directory. Then we update the submodule
    /// for the given project according to the latest commit (XXX should we wire in the exact sha?).
    /// Next, we will create a merge request for the master project, thus kicking off the 
    /// appropriate pipelines/test deployments
    fn update_submodule(&self, project_name: &str, sha: &str, config: &Config) {
        let master_dir = TempDir::new("onboard_master_project")
            .expect("Could not create a temporary directory to clone master project into");

        let master_project = config.git_master_project.as_ref()
            .expect("Attempting to update master project, but git_master_project is undefined");

        // the project may have namespaces in it 
        // (TODO this wouldn't really work if it did, because of the builder below)
        let master_project_name = master_project.split('/').last()
            .expect("Invalid git_master_project value");

        // use git_ssh_host if defined, otherwise, we default to the git_host
        let ssh_host = get_git_ssh_host(config);
        let git_path= format!("git@{}:{}/{}.git", 
            ssh_host,
            config.gitlab_user,
            master_project_name
        );

        // We don't know where the master project is checked out. If we don't pull in the submodules,
        // the expectation is that this project would be small, so we'll just clone them into a 
        // temporary directory to do our work on rather than forcing them to add yet another config
        // value during ingest. The TempDir will ensure the directory is deleted when it goes out of
        // scope
        run_shell! { master_dir.path() =>
            "Clone the master project into the temporary directory" =>
                "git", "clone", &git_path, "--depth", "1", master_project_name
        }

        // this is the directory that the actual checkout is now in
        let project_checkout_dir = master_dir.path().join(master_project_name);
        let branch_name = format!("onboard_{}_{}", project_name, sha);

        run_shell! { &project_checkout_dir =>

            // We'll create a branch in which to make a merge request for. This will allow us to
            // reject updates easily if something goes wrong
            "Create Git Branch for this ingest" =>
                "git", "checkout", "-b", &branch_name;

            // Now run `git submodule update $project` to get the new ref
            &format!("Updating the submodule for: {}", project_name) =>
                "git", "submodule", "update", "--init", "--remote", project_name;

            // commit the changes
            "Updating the master project with new submodule revision" =>
                "git", "commit", "-am", "'onboard submodule update'";

            // push the update
            "Pushing the updated submodule to the master project" => 
                "git", "push", "-u", "origin", &branch_name
        };

        // Create the merge request for the new branch
        //
        // Build up the REST endpoint location
        // The projects name is the URL-encoded namespace/project_path (%2F is /)
        let url = format!("{}://{}/api/v4/projects/{}%2F{}/merge_requests",
            config.gitlab_proto,
            config.git_host.as_ref().expect("Git host not specified in the config file"), 
            config.gitlab_user,
            master_project_name);

        let gitlab_token = config.gitlab_token.as_ref()
            .expect("No git token was specified in the config file");

        // Make the request to the REST server
        println!("Creating the merge request for {} into master", branch_name);
        let client = reqwest::Client::builder()
            .danger_accept_invalid_certs(true)
            .build()
            .expect("Could not build reqwest client for REST requests to Git");
        let mut params = BTreeMap::new();
        params.insert("source_branch", branch_name);
        params.insert("target_branch", "master".to_owned());
        params.insert("title", format!("Onboard {} - {} update", project_name, sha));
        params.insert("remove_source_branch", "true".to_owned());
        params.insert("squash", "true".to_owned());

        let mut resp = client.post(url.as_str())
            .header("PRIVATE-TOKEN", gitlab_token)
            .json(&params)
            .send()
            .expect("Error querying git to see if the project already exists");

        // Process the result, expecting to get success for creation
        if !resp.status().is_success() {
            println!("{:?}", resp.text());
            panic!("Failed to create merge request");
        }
    }
}

fn discover_remote_url(out_dir: &Path) -> String {
    run_shell! { out_dir =>
        "Auto-discovering remote 'origin' URL from git (was not specified in the config)" =>
            let remote = "git", "config", "--get", "remote.origin.url"
    }
    remote
}

fn discover_current_branch(out_dir: &Path) -> String {
    run_shell! { out_dir =>
        "Auto-discovering current branch from Git (was not specified in the config)" =>
            let branch = "git", "rev-parse", "--abbrev-ref", "HEAD"
    }
    branch
}

fn get_git_ssh_host(config: &Config) -> String {
    // use git_ssh_host if defined, otherwise, we default to the git_host
    let ssh_host = config.git_ssh_host.as_ref()
        .or_else(|| config.git_host.as_ref())
        .expect("Neither git_ssh_host or git_host defined");
    ssh_host.clone()
}
