//! Module for interacting with docker
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::process::{Command, Output};
use uuid::Uuid;
use crate::description_defs::Containers;
use crate::path_extensions::PathExtensions;

pub trait ContainerRepo {
    /// Pushes a given image to the given repository
    ///
    /// # Arguments
    ///
    /// * `image_name` - The local image name to be pushed
    /// * `repo` - The url to the remote repository (e.g mydocker-repo:5001)
    /// Note: this drops to a shell to run docker tag and push commands
    fn push_image(&self, image_name: &str, repo: &str);

    /// Given a container name, saves the layers of the images in a format
    /// consumable by docker load.
    /// 
    /// This command uses (or is equivalent to)
    /// ```sh
    /// > cd {output_directory}
    /// > docker save -o {random_name}
    /// ```
    /// 
    /// The random name is returned upon success.
    /// 
    /// Auto-pull is useful when building containers locally. Skip auto-pull for containers that
    /// are built, and pull ones that will be updated remotely or absent locally.
    /// 
    /// *Examples*
    /// ```rust
    /// // Built locally
    /// let ui_tar_name = repo.package_containers("my-ui:1.5", auto_pull=False, output_directory=Path::new("."));
    /// // Need to grab a copy from a remote repository
    /// let postgres_tar_name = repo.package_containers("postgres:12.0", auto_pull=True, output_directory=Path::new("."));
    /// ```
    fn package_containers(&self, containers: &[&Containers], output_directory: &Path) -> Option<String>;
    
    /// Takes a saved docker image file and loads it
    /// to the docker daemon.
    /// 
    /// This load uses (or is equivalent to): `docker load -i {filename}`
    fn load_image(&self, filename: &Path);

    /// Normalized the docker name to strip off the specified
    /// hostname. This will help to target the private repositories
    /// on the system without needing to worry about repo names
    fn normalize_name(&self, name: String, registry_name: Option<&str>) -> String;
}

pub struct DockerRepo;

impl ContainerRepo for DockerRepo {
    fn push_image(&self, image_name: &str, repo: &str) {
        let remote_image_name = format!("{}/{}", repo, self.normalize_name(image_name.to_owned(), None));

        run_shell! { "." =>
            format!("Tagging '{}' to '{}'", image_name, repo) =>
                "docker", "tag", image_name, &remote_image_name;
            format!("Pushing '{}' to '{}'", image_name, repo) =>
                "docker", "push", &remote_image_name
        };
    }

    fn package_containers(&self, containers: &[&Containers], output_directory: &Path) -> Option<String> {
        if containers.is_empty() {
            println!("No docker images defined, skipping save");
            return None ;
        }
        let save_name = format!("{}.tar", Uuid::new_v4().to_simple().to_string());
        for container in containers {
            // Build using the dockerfile if one is specified, otherwise, we will
            // assume we can pull the image
            if let Some(dockerfile) = &container.dockerfile {
                println!("Building docker image: {}", container.name);

                // If the dockerfile specification starts with %project, we handle
                // the %project as a reference to a repository. That means, we
                // will look inside of our .onboard/package staging directory for the path.
                // Otherwise, we treat it as a relative path to the current project
                let dockerfile_transformed = if dockerfile.starts_with('%') {
                    output_directory.join(dockerfile.replace('%', ""))
                } else {
                    PathBuf::from(dockerfile).absolute_path()
                };

                // The context for the docker build is the parent directory for the dockerfile
                let basedir = dockerfile_transformed.parent();
                let basedir_str = basedir
                    .expect("No basedir found for the dockerfile")
                    .to_string_lossy();
                
                // Execute the docker build with the specified container name, and dockerfile/context
                run_shell! { "." =>
                    "Build docker image" =>
                        "docker", "build", "-t", &container.name, 
                        "-f", &*dockerfile_transformed.to_string_lossy(), &*basedir_str
                }
            } else {
                // run_shell doesn't support conditional failing
                println!("Pulling docker image: {}", container.name);
                let pull_output = Command::new("docker")
                    .args(&["pull", &container.name])
                    .output()
                    .expect("Failed to pull the docker image");
                if !container.allow_pull_failure {
                    handle_output(pull_output);
                }
            }
        }

        let mut save_args: Vec<&str> = vec!["save", "-o", &save_name];
        let cnt_list: &Vec<&str> = &containers.iter().map(|c| c.name.as_ref()).collect();
        save_args.extend(cnt_list);

        run_shell! { output_directory =>
            format!("Saving docker image: {:?}", cnt_list) =>
                "docker" [save_args]
        }

        // return the saved name
        Some(save_name)
    }

    fn load_image(&self, filename: &Path) {
        run_shell! { "." =>
            "Load docker image" =>
                "docker", "load", "-i", filename.to_str().unwrap()
        }
    }

    fn normalize_name(&self, name: String, registry_name: Option<&str>) -> String {
        normalize_img_name(name, registry_name)
    }
}


pub fn normalize_img_name(name: String, registry_name: Option<&str>) -> String {
    // Based off of docker's implementation to parse the names
    // https://github.com/docker/distribution/blob/f18781257ec94f25301f1f546b2e4b90612295e7/reference/normalize.go#L91
    // 
    // Adapted to our needs to leave off the docker default domains (i.e docker.io)
    let slash = name.find('/');
    let new_registry = match registry_name {
        Some(reg_name) => reg_name.trim_end_matches('/').to_owned() + "/",
        None => "".to_owned()
    };
    if let Some(slash_index) = slash {
        let domain = &name[..slash_index];
        // there is a slash, check if it looks like a hostname
        if domain == "localhost" || domain.chars().any(|c| c == '.' || c == ':') {
            // adding 1 removes the /
            return new_registry + &name[slash_index+1..];
        } 
    }

    // there is no slash anywhere, and nothing that appears to be a hostname
    // Note: it appears a shortname host with no port will be assumed to be docker.io
    // even if it is a username
    new_registry + name.as_ref()
}

// XXX there is an opportunity to check if the images of this
// version of the image is already in the repo. The idea is we want to
// fail if you try uploading a new version of an existing tag, but it would ok
// to declare an identical image in your package. Docker implements this with its manifest/digest
//
// pullV2Tag method in https://github.com/moby/moby/blob/e582a10b593c80c54527ae01e5e875ca6b353ac2/distribution/pull_v2.go

fn handle_output(output: Output) {
    if !output.status.success() {
        io::stdout().write_all(&output.stdout).unwrap();
        io::stderr().write_all(&output.stderr).unwrap();
        panic!("Docker Command Failed");
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_normalize_name() {
        let repo = DockerRepo{};
        assert_eq!("something:latest", repo.normalize_name("something:latest".to_owned(), None));
        assert_eq!("something:latest", repo.normalize_name("repo.test/something:latest".to_owned(), None));
        assert_eq!("something:latest", repo.normalize_name("repo:5000/something:latest".to_owned(), None));
        assert_eq!("something:latest", repo.normalize_name("repo.test:5000/something:latest".to_owned(), None));
        assert_eq!("something/else:latest", repo.normalize_name("something/else:latest".to_owned(), None));
        assert_eq!("something/else:latest", repo.normalize_name("docker.io/something/else:latest".to_owned(), None));
        assert_eq!("something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), None));
        assert_eq!("something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), None));
    }

    #[test]
    fn test_normalize_name_with_new_host() {
        let repo = DockerRepo{};
        assert_eq!("new.repo/something:latest", repo.normalize_name("something:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something:latest", repo.normalize_name("repo.test/something:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something:latest", repo.normalize_name("repo:5000/something:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something:latest", repo.normalize_name("repo.test:5000/something:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something/else:latest", repo.normalize_name("something/else:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something/else:latest", repo.normalize_name("docker.io/something/else:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), Some("new.repo")));
        assert_eq!("new.repo/something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), Some("new.repo")));

        assert_eq!("new.repo/something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), Some("new.repo/")));
        assert_eq!("new.repo:5000/something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), Some("new.repo:5000")));
        assert_eq!("new.repo:5000/something.else/entirely:latest", repo.normalize_name("repo.test/something.else/entirely:latest".to_owned(), Some("new.repo:5000/")));
    }
}