//! Module for interacting with helm repositories
use flate2::write::GzEncoder;
use flate2::Compression;
use std::collections::BTreeMap;
use std::env::var;
use std::error::Error;
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use regex::Regex;

use reqwest::Body;
use std::fs::File;
 
type CmdResult<T> = Result<T, Box<dyn Error>>;

// We don't need to store any state here, continue avoiding it
pub struct Helm;

impl HelmCommand for Helm {
    /// Packages the chart for distribution
    /// Equivalent to: helm package {chart_path} -d {build_dir} --save=true
    fn package_chart(chart_path: &Path, build_dir: &Path) -> CmdResult<PathBuf> {
        if !chart_path.is_dir() {
            panic!("Helm chart definition '{}' is not a directory", chart_path.to_string_lossy());
        }
        let chart_yaml_path = chart_path.join("Chart.yaml");
        // NIT lazy create these errors strings
        let f = std::fs::File::open(&chart_yaml_path)
            .unwrap_or_else(|_| panic!("Cannot open Chart.yaml inside '{}'", chart_path.to_string_lossy()));
        let chart_yaml: HelmChartYaml = serde_yaml::from_reader(f)
            .unwrap_or_else(|_| panic!("Cannot parse yaml on Chart.yaml in '{}'", chart_path.to_string_lossy()));
        let chart_name = chart_yaml.name.replace('-', "_");

        // since we aren't using any PGP portions of the package command, we can
        // use tar directly and not depend on helm being installed on the box
        let tar_path = build_dir.join(format!("{}-{}.tgz", chart_name, chart_yaml.version));
        let tar_gz = File::create(&tar_path)?;
        let mut encoder = GzEncoder::new(tar_gz, Compression::default());

        {
            // Create tar archive and compress files
            let mut archive = tar::Builder::new(&mut encoder);
            archive.append_path_with_name(chart_yaml_path, format!("{}/Chart.yaml", chart_name))?;
            archive.append_path_with_name(chart_path.join("values.yaml"), format!("{}/values.yaml", chart_name))?;

            let templates_dir = chart_path.join("templates");
            if templates_dir.exists() {
                archive.append_dir_all(format!("{}/templates", chart_name), chart_path.join("templates"))?;
            }

            let charts_dir = chart_path.join("charts");
            if charts_dir.exists() {
                archive.append_dir_all(format!("{}/charts", chart_name), chart_path.join("charts"))?;
            }
        }

        // Finish Gzip file
        encoder.finish()?;
        Ok(tar_path)
    }

    /// Returns a list of docker images found inside the template
    /// 
    /// Packages up all images discovered in a helm chart. We first execute `helm template` so
    /// that we get the rendered version of the chart (taking into account any guards)
    fn package_helm_imgs(chart_path: &Path, forward_vars: &BTreeMap<String, String>, skip_dep_update: bool) -> Vec<String> 
    {
        let chart_path_name = chart_path.to_str().expect("Could not convert chart parth to str");
        if ! skip_dep_update {
            run_shell! { chart_path_name => 
                &format!("Updating helm dependencies (assumes helm >=3): {}", chart_path_name) =>
                   "helm", "dependency", "update", chart_path_name
            }
        }
        
        // Run helm template to give us a rendered view of the k8s manifest that will be used.
        // We can use this to parse the images out for the deployment.
        //
        // not using run_shell since we process lines, and it isn't much cleaner in this case
        let template_output = Command::new("helm")
            .arg("template")
            .arg("-g")
            .arg(chart_path_name)
            .args(generate_helm_vals_from_env(forward_vars))
            .stderr(Stdio::piped())
            .output()
            .expect("Failed to run helm template");
        if !template_output.status.success()
        {
            println!();
            io::stdout().write_all(&template_output.stdout).unwrap();
            io::stdout().write_all(&template_output.stderr).unwrap();
            panic!(format!("Error running helm template on chart (assumes helm >=3): {}", chart_path_name));
        }
        lazy_static! {
            static ref IMG_RE: Regex = Regex::new(r#"^\s*-?\s*image:\s+"?([^"]+)"?$"#).unwrap();
        }
        // XXX probably prefer to stream line by line...
        String::from_utf8(template_output.stdout)
            .expect("Could not slurp output from helm template")
            .lines()
            .filter_map(|l| {
                if let Some(captures) = IMG_RE.captures(l) {
                    if let Some(img_name) = captures.get(1) {
                        let img_name_str = img_name.as_str();
                        // some templates may define image: with template
                        // placeholders that throw off this logic and do not
                        // render out with helm template. For example, istio
                        // does this with their injection templates.
                        // TODO determine if this is excluding any critical images that
                        // are dynamically generated in istio
                        //
                        // There is only so much we can do in future projects, and they
                        // can always manually include these image names in the containers
                        // portion of the onboard.yaml if needed
                        if img_name_str.contains("{{") {
                            return None;
                        }
                        return Some(String::from(img_name.as_str()));
                    }
                }
                None
            })
            .collect()
    }
}

pub fn generate_helm_vals_from_env(mapping: &BTreeMap<String, String>) -> Vec<String> {
    let mut command_list: Vec<String> = vec![];
    for (k, v) in mapping {
        let env_value = var(k).unwrap_or_else(|_| {
            let msg = format!("Environment variable '{}' required but not set", k);
            panic!(msg);
        });
        command_list.push("--set".to_owned());

        command_list.push(format!("{}={}", v, env_value));
    }
    command_list
}

pub trait HelmRepo<F: FileTrait, R: Request> {
    fn push_chart<P: AsRef<Path>>(chart_path: P) -> CmdResult<HelmChartYaml> {
        // parse the name/version out of the filename
        let filename = String::from(
            chart_path
                .as_ref()
                .file_stem()
                .expect("Invalid chart path")
                .to_string_lossy(),
        );
        let split_str: Vec<&str> = filename.split('-').collect();
        assert!(split_str.len() == 2, "Invalid chart path, filename must be in the format of: name-version.tgz");

        let descriptor = HelmChartYaml {
            name: split_str[0].to_owned(),
            version: split_str[1].to_owned(),
        };
        let pkg = F::open(chart_path.as_ref())?;
        let client = R::new();
        client
            .post("http://pipeline-chartmuseum:8080/api/charts")
            .body(pkg)
            .send()?;
        Ok(descriptor)
    }
}
 
impl HelmRepo<File, ReqwestClient> for Helm {}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct HelmChartYaml {
    pub name: String,
    pub version: String,
}


pub trait Request {
    type RB: RequestBuilder;

    fn new() -> Self;
    fn post<U: reqwest::IntoUrl>(self, url: U) -> Self::RB;
}
pub trait RequestBuilder {
    fn body<B: Into<Body>>(self, body: B) -> Self;
    fn send(self) -> CmdResult<()>;
}

pub struct ReqwestClient(reqwest::Client);
pub struct ReqwestBuilder(reqwest::RequestBuilder);

impl Request for ReqwestClient {
    type RB = ReqwestBuilder;

    fn new() -> Self {
        ReqwestClient(reqwest::Client::new())
    }
    fn post<U: reqwest::IntoUrl>(self, url: U) -> ReqwestBuilder {
        ReqwestBuilder(self.0.post(url))
    }
}

impl RequestBuilder for ReqwestBuilder {
    fn body<B: Into<Body>>(self, body: B) -> Self {
        let builder = self.0.body(body);
        ReqwestBuilder(builder)
    }
    fn send(self) -> CmdResult<()> {
        match self.0.send() {
            Ok(_a) => Ok(()),
            Err(e) => Err(Box::new(e))
        }
    }
}

pub trait HelmCommand {
    /// Packages a chart directory into a tgz using the 'helm package' command.
    /// Returns a Path to the tgz upon success.
    ///
    fn package_chart(chart_path: &Path, build_dir: &Path) -> CmdResult<PathBuf>;

    // Looks at the specified helm chart and returns the list of docker image
    // names required by that chart
    fn package_helm_imgs(chart_path: &Path, forward_vars: &BTreeMap<String, String>, skip_dep_update: bool) -> Vec<String>;
}


pub trait FileTrait {
    type ContentType: Into<Body>;

    fn open(p: &Path) -> io::Result<Self::ContentType>;
}

// Used to provide interface for tests
impl FileTrait for File {
    type ContentType = File;
    fn open(p: &Path) -> io::Result<Self::ContentType> {
        File::open(p)
    }
}


#[cfg(test)]
pub mod tests {
    use super::*;

    struct FakeFile;
    impl FileTrait for FakeFile {
        type ContentType = String;
        fn open(_p: &Path) -> io::Result<Self::ContentType> {
            // can lookup contents on path if needed, think empty string is fine
            Ok(String::from(""))
        }
    }

    #[derive(Default)]
    struct FakeRequest {
        #[allow(dead_code)]
        url: Option<String>,
        #[allow(dead_code)]
        body: Option<Body>
    }
    impl Request for FakeRequest {
        type RB = FakeRequest;
        fn new() -> Self {
            FakeRequest{..Default::default()}
        }
        fn post<U: reqwest::IntoUrl>(self, url: U) -> FakeRequest {
            let url = Some(url.into_url().unwrap().as_str().into());
            FakeRequest{url, ..self}
        }
    }
    impl RequestBuilder for FakeRequest {
        fn body<B: Into<Body>>(self, body: B) -> Self {
            let body = Some(body.into());
            FakeRequest{body, ..self}
        }
        fn send(self) -> CmdResult<()> {
            Ok(())
        }
    }
    struct HelmTest{}
    impl HelmRepo<FakeFile, FakeRequest> for HelmTest {}

    #[test]
    #[should_panic(expected="Invalid chart path")]
    fn test_push_chart_empty_path_string() {
        let p = PathBuf::from("");
        let _yaml = HelmTest::push_chart(p).unwrap();
    }

    #[test]
    #[should_panic(expected="Invalid chart path, filename must be in the format of: name-version.tgz")]
    fn test_push_chart_non_conforming_path_pattern() {
        let p = PathBuf::from("justFile");
        let _yaml = HelmTest::push_chart(p).unwrap();
    }
    
    #[test]
    fn test_push_chart_no_file_contents() {
        let p = PathBuf::from("chart-5.5.tgz");
        let _yaml = HelmTest::push_chart(p).unwrap();
    }

    #[test]
    #[should_panic(expected="Invalid chart path, filename must be in the format of: name-version.tgz")]
    fn test_push_chart_non_filename_too_many_hyphens() {
        let p = PathBuf::from("chart-5.5.5-2.2.tgz");
        let _yaml = HelmTest::push_chart(p).unwrap();
    }

    #[test]
    fn test_push_chart_valid_package() {
        let p = PathBuf::from("a-0.1.tgz");
        let yaml = HelmTest::push_chart(p).unwrap();

        let expected = HelmChartYaml {
            name: "a".to_string(),
            version: "0.1".to_string(),
        };
        assert_eq!(expected, yaml);
        // TODO can I check what the url and body were in the fake request were? maybe a closure
    }

}
