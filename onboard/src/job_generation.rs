use std::fs::{File};
use std::collections::{BTreeMap, HashSet};
use std::path::{Path, PathBuf};
use crate::repository::git::{GitRepo};
use crate::repository::{ArtifactRepository};
use crate::creation::{discover_helm_images};
use crate::description_defs::{CreationDef, ManifestGitRepo};

macro_rules! stages_def {
    ($($x:expr,)*) => (vec![$($x),*].iter().map(|s| String::from(*s)).collect())
} 

/// Creates gitlab-ci.yml files from the submodules for all given tasks. The files will be placed in
/// the specified output directory.
pub fn create_job_definitions<AR: ArtifactRepository>(repo_directory: &Path, output_directory: &Path, repository: &AR) {
    assert!(repo_directory.is_dir(), "The specified repository directory is not a directory");
    assert!(repo_directory.is_dir(), "The specified output directory is not a directory");

    // Note: not handling the sub repos here. I see that more as a packaging convenience than
    // dealing with the repo deployment itself
    let submodules = repository.git_repo().list_submodules(repo_directory);
    create_pylint_jobs(repo_directory, output_directory, &submodules);
    create_deployment_jobs(repo_directory, output_directory, &submodules);
    create_integration_tests(repo_directory, output_directory, &submodules);
    create_scanning_jobs(repo_directory, output_directory, &submodules);
}

fn create_pylint_jobs(repo_directory: &Path, output_directory: &Path, submodules: &[String]) {
    assert!(repo_directory.join("pylint.rc").exists(), 
        "No 'pylint.rc' file exists in the main repository directory. Consider using 'pylint --generate-rcfile' to create one");

    let stage = "pylint".to_owned();
    let mut ci_file = CIFile {
        variables: BTreeMap::new(),
        stages: vec![stage.clone()],
        jobs: BTreeMap::new()
    };
    for module in submodules {
        let job = CIScriptJob {
            stage: stage.clone(),
            image: "docker.io/mfilipiakccg/pylint:3.7".to_owned(),
            script: vec![
                format!("git submodule update --init {}", module),
                format!("pylint --rcfile pylint.rc -j 4 -s n {}", module)
            ]
        };
        ci_file.jobs.insert(format!("pylint_{}", module), job);
    }
    let out_file_handle = File::create(output_directory.join("pylint.yml"))
        .expect("Could not open pylint output file");
    serde_yaml::to_writer(out_file_handle, &ci_file).expect("Could not write pylint output file");

    println!("Created pylint jobs for {} submodules", submodules.len());
}

fn create_deployment_jobs(repo_directory: &Path, output_directory: &Path, submodules: &[String]) {
    let stage = "deploy".to_owned();
    let mut ci_file = CIFile {
        variables: BTreeMap::new(),
        stages: vec![stage.clone()],
        jobs: BTreeMap::new()
    };
    for module in submodules {
        let onboard_def  = load_onload_file(repo_directory, module);

        // TODO not supporting child repos here yet. it isn't as simple as just chaining them,
        // we also would need to clone the repo
        for chart in onboard_def.repo.helm {
            // add in any variables that they need forwarded to the helm chart
            // They are declared in the onboard.yaml as `ENV_VARIABLE: helm_variable_name`.
            let helm_set_vars: Vec<String> = chart.forward_vars.iter()
                .map(|(k,v)| format!("--set {}=\"${}\"", v, k)).collect();
            let helm_set_vars_str = helm_set_vars.join(" ");

            // build the list of commands, optionally labeling the namespace for istio sidecar
            // injection
            let mut script_list = vec![
                format!("git submodule update --init {}", module),
                format!("cd {}", module),
                format!("kubectl create namespace {} ||", module),
            ];
            if ! chart.disable_sidecar_injection {
                script_list.push(format!(
                    "kubectl label namespace {} istio-injection=enabled --overwrite",
                    module
                ));
            }
            script_list.push(format!(
                "helm upgrade -i {} --force --post-renderer onboard_env_normalize.sh \
                --namespace {} {} {}",
                module,
                module,
                chart.chart.to_string_lossy(),
                helm_set_vars_str
            ));

            let job = CIScriptJob {
                stage: stage.clone(),
                image: "$ONBOARD_IMAGE".to_owned(),
                script: script_list,
            };


            ci_file.jobs.insert(format!("deployment_{}", module), job);
        }
    }
    let out_file_handle = File::create(output_directory.join("deployment.yml"))
        .expect("Could not open deployment output file");
    serde_yaml::to_writer(out_file_handle, &ci_file).expect("Could not write deployment output file");

    println!("Created deployment jobs for {} submodules", submodules.len());
}

fn create_integration_tests(repo_directory: &Path, output_directory: &Path, submodules: &[String]) {
    // We initially designed this to trigger/include to reference this file from the
    // remote repo. At this time, it does not appear that gitlab will allow multiple level
    // triggers (or I couldn't figure out how to do it). This would be ideal since it would
    // sandbox things like job names etc per project.
    // 
    // We then attempted to use an include to do that same thing, without success. I've
    // settled on just copying the contents in (and as consolation, we can verify job names and
    // do basic checking to avoid errors in a fail fast style)
    let mut stages_graph = Vec::new();
    let mut master_yaml = serde_yaml::Mapping::new();
    // TODO this doesnt only work on jobs, but we also don't merge anything else either, so best
    // to fail on those too for now
    let mut keys: HashSet<serde_yaml::Value> = HashSet::new();

    // Since we are referencing the submodule locally, we need to pull the
    // submodules. 
    // XXX It would be nicer to pull only the relevant module, sub I
    // haven't thought that through completely
    let mut variables = serde_yaml::Mapping::new();
    variables.insert(
        serde_yaml::Value::String("GIT_SUBMODULE_STRATEGY".to_owned()), 
        serde_yaml::Value::String("recursive".to_owned())
    );
    master_yaml.insert(serde_yaml::Value::String("variables".to_owned()), serde_yaml::Value::Mapping(variables));

    for module in submodules {
        let onboard_def  = load_onload_file(repo_directory, module);

        //collect all the tests in this repo
        for t in onboard_def.repo.tests {
            // TODO we can detect job name collisions and error out
            let def = transform_job_definitions(&PathBuf::from(module).join(t), &module, &mut stages_graph);

            // merge the whole thing into the master yaml
            for (k, v) in def.iter() {
                if keys.contains(k) {
                    panic!("The entry '{:?}' is a duplicate!", k);
                }
                keys.insert(k.clone());
                master_yaml.insert(k.clone(), v.clone());
            }
        }
    }

    let stage_values: Vec<serde_yaml::Value> = stages_graph.iter()
        .map(|s| serde_yaml::Value::String(s.clone()))
        .collect();
    master_yaml.insert(serde_yaml::Value::String("stages".to_owned()), serde_yaml::Value::Sequence(stage_values));

    // write out the merged file
    let out_file_handle = File::create(output_directory.join("integration_tests.yml"))
        .expect("Could not open tests output file");
    serde_yaml::to_writer(out_file_handle, &master_yaml).expect("Could not write integration output file");


    println!("Created tests jobs for {} submodules", submodules.len());
}

/// Go through an existing GitLab CI file and massage it into a format we can consume and merge from
fn transform_job_definitions(file_location: &Path, module: &str, stage_graph: &mut Vec<String>) -> serde_yaml::Mapping {
    let f = File::open(file_location).expect("Cannot open file");
    let mut content: serde_yaml::Value = serde_yaml::from_reader(f).expect("Cannot parse yaml");

    let root = content.as_mapping_mut().unwrap();

    // We want to generate a list of stages so we can make a master list of them
    // We need to make sure our merging doesn't disturb any dependencies that may exist
    let stages: Vec<String> = match root.remove(&serde_yaml::Value::String("stages".to_owned())) {
        Some(stages) => {
            stages.as_sequence()
                .expect("Stages was not a list")
                .iter()
                .map(|s| s.as_str().expect("Stage value was not a string").to_owned())
                .collect()
        },
        // these are the defaults in gitlab, so make these available if nothing is defined
        None => stages_def![".pre", "build", "test", "deploy", ".post",]
    };

    // add our stages to the target stages def
    merge_stage_graph(&stages, stage_graph);

    // now go through and add a command to change directories from root into the submodule
    //
    // This is not elegant, and is really a workaround for gitlab not seeming to support
    // running a 2 job trigger (generated job -> external project non-gitlab.yml file).
    let mut detected_stages: Vec<String> = vec![];
    for (_k, v) in root.iter_mut() {
        if let Some(entry) = v.as_mapping_mut() {
            // only work on job defintions that have 'script' in it, this should leave
            // other utility jobs untouched (e.g. variables, stages)
            // TODO this might not work as expected if there is a before script defined 
            // that expects to be in the repo dir (worst case if they can add it themselves)
            if let Some(script) = entry.get_mut(&serde_yaml::Value::from("script").clone()) {
                let script_list = script.as_sequence_mut().expect("Script tag is not a list");
                // the 'cd' command will be injected at the beginning of the script list
                script_list.insert(0, serde_yaml::Value::from(format!("cd {}", module)));
            }
        }
        if let Some(stage) = v.get("stage") {
            detected_stages.push(stage.as_str().expect("Stage was not a string").to_owned());
        }
    }

    // collect the stages that were detected in the job definitions, but not declared
    // in the stages def (or able to be defaulted). We need them to build up our stages graph
    let undefined_stages: Vec<&String> = detected_stages.iter()
        .filter(|s| stages.contains(s))
        .collect();

    // There should be no undefined stages, otherwise this is an error
    if ! undefined_stages.is_empty() {
        panic!("The following stages were not declared {:?}", undefined_stages);
    }

    // TODO can i avoid this clone in a nice way?
    root.clone()
}

/// Merge the new stages into the existing one, while not breaking the dependency
/// graphs of either definition
/// 
/// The algorithm is rather simple. We make a first pass through the stages being merged in
/// and create 'fence' indexes for items that already exist. We then make sure no indexes go
/// backwards, because that would break dependencies some other jobs may have defined (panic).
/// Then we just insert our new jobs safely before these defined fences
fn merge_stage_graph(new_stages: &[String], defined_stages: &mut Vec<String>) {
    // this gives us a list of positions that are fixed according to already defined
    // stages. In short, the positions where the these stages already exist
    let fixed_indexes: Vec<Option<usize>> = new_stages.iter()
        .map(|stage| defined_stages.iter().position(|s| stage == s))
        .collect();

    // verify none of these stages go in the wrong direction, that would break the dependencies
    // defined by another definition
    let mut prev_index: usize = 0;
    for i in fixed_indexes.iter() {
        if let Some(index) = i {
            if *index < prev_index {
                panic!("Could not merge stages with other stage definitions. Conflict with {}, {}",
                    new_stages[prev_index],
                    new_stages[*index]
                );
            }
            prev_index = *index;
        }
    }

    // find valid places to put the stage (if not already in the list), and insert them
    let mut insert_index: usize = 0;
    // since we are inserting into the list (all before these calculated indexes), we are keeping
    // track of how many we need to offset by
    let mut inserted_cnt = 0;
    for (stage, index) in new_stages.iter().zip(fixed_indexes.iter()) {
        if let Some(next_index) = index {
            // we want the next one that isn't in the list already to sit
            // directly after this one.
            insert_index = next_index + inserted_cnt + 1;
        } else {
            defined_stages.insert(insert_index, stage.clone());
            insert_index += 1;
            inserted_cnt += 1;
        }
    }
}

fn create_scanning_jobs(repo_directory: &Path, output_directory: &Path, submodules: &[String]) {
    let stage = "scanning".to_owned();
    let mut ci_file = CIFile {
        variables: BTreeMap::new(),
        stages: vec![stage.clone()],
        jobs: BTreeMap::new()
    };
    for module in submodules {
        let onboard_def  = load_onload_file(repo_directory, module);

        // First make sure the paths in the submodule directory are relative
        // to the project they live in
        let helm_defs_rerooted = onboard_def.repo.helm
            .iter()
            .map(|helm_def| {
                let mut def = helm_def.clone();
                def.chart = PathBuf::from(module).join(def.chart);
                def
            })
            .collect();

        let manifest = vec![
            // partially populate the manifest to point to our submodules
            ManifestGitRepo {
                helm: helm_defs_rerooted,
                ..Default::default()
            }
        ];

        let container_list = discover_helm_images(&onboard_def, &manifest, &PathBuf::from("."))
            .iter()
            .chain(onboard_def.containers.iter())
            .fold(String::new(), |mut acc, c| {
                acc.push_str(" ");
                acc.push_str(&c.name);
                acc
            });

        if ! container_list.is_empty() {
            let job = CIScriptJob {
                stage: stage.clone(),
                image: "python:3.7-alpine".to_owned(),
                script: vec![
                    format!("echo {}", container_list)
                ]
            };
            ci_file.jobs.insert(format!("container_scanning_{}", module), job);
        }
    }
    let out_file_handle = File::create(output_directory.join("container_scanning.yml"))
        .expect("Could not open scanning output file");
    serde_yaml::to_writer(out_file_handle, &ci_file).expect("Could not write scanning output file");
    println!("Created container scanning jobs for {} submodules", submodules.len());
}

fn load_onload_file(repo_directory: &Path, module: &str) -> CreationDef {
    // grab the submodule onboard.yaml file from the submodule directory
    let onboard_path = repo_directory.join(module).join("onboard.yaml");
    assert!(onboard_path.exists(), "No onboard defintion found for {}", module);

    // parse it into a creation def
    let f = File::open(onboard_path).expect("Cannot open file");
    let onboard_def: CreationDef = serde_yaml::from_reader(f).expect("Cannot parse yaml");
    onboard_def
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CIFile<T: CIJob> {
    pub variables: BTreeMap<String, String>,
    pub stages: Vec<String>,

    #[serde(flatten)]
    pub jobs: BTreeMap<String, T>
}

pub trait CIJob {}
impl CIJob for CIScriptJob {}
impl CIJob for CITriggerJob {}
impl CIJob for Vec<String> {}

#[derive(Debug, Deserialize, Serialize)]
pub struct CIScriptJob {
    pub stage: String,
    pub image: String,
    pub script: Vec<String>
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CITriggerJob {
    pub stage: String,
    pub trigger: CITriggerDef,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CITriggerDef {
    pub include: String,
    pub strategy: String
}


#[cfg(test)]
pub mod tests {
    use super::*;
    
    #[test]
    fn test_merge_stages_disjoint() {
        let mut stages_accum: Vec<String> = stages_def!["a", "b",];
        let stages_new: Vec<String> = stages_def!["c", "d",];

        let expected: Vec<String> = stages_def!["c", "d", "a", "b",];
        merge_stage_graph(&stages_new, &mut stages_accum);
        assert_eq!(expected, stages_accum);
    }

    #[test]
    fn test_middle_merge_stages() {
        let mut stages_accum: Vec<String> = stages_def!["a", "b", "c",];
        let stages_new: Vec<String> = stages_def!["a", "a1", "b",];

        let expected: Vec<String> = stages_def!["a", "a1", "b", "c",];
        merge_stage_graph(&stages_new, &mut stages_accum);
        assert_eq!(expected, stages_accum);
    }

    #[test]
    #[should_panic(expected = "Could not merge stages with other stage definitions. Conflict with a, b")]
    fn test_merge_stages_inverted() {
        let mut stages_accum: Vec<String> = stages_def!["a", "b",];
        let stages_new: Vec<String> = stages_def!["b", "a",];

        merge_stage_graph(&stages_new, &mut stages_accum);
    }

    #[test]
    fn test_merge_longer_sequence() {
        let mut stages_accum: Vec<String> = stages_def!["a", "b", "c", "d", "e", "f", "g", "h",];
        let stages_new: Vec<String> = stages_def!["0", "1", "c", "2", "3", "d", "4", "h", "5",];

        let expected: Vec<String> = stages_def![
            "0", "1", "a", "b", "c", "2", "3", "d", 
            "4", "e", "f", "g", "h", "5",
        ];
        merge_stage_graph(&stages_new, &mut stages_accum);
        assert_eq!(expected, stages_accum);
    }
}