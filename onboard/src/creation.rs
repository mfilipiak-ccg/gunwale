//! The main entry point for creating a package. 
//! 
//! This is intended to be used by the vendor prior to delivery. They will specify their 
//! artifacts and the tool will package them up.
//! 
//! See [CreationDef](struct.CreationDef.html) for specifying this file.
use std::io::{Read};
use std::fs::{create_dir_all, remove_dir_all, File};
use std::path::{Path, PathBuf};

use crate::helm::{Helm, HelmCommand};
use crate::description_defs::{DockerContainers, ManifestGitRepo, Manifest, GitRepo};
use crate::repository::ArtifactRepository;
use crate::path_extensions::PathExtensions;

// these are needed to use trait methods, aliased to avoid collisions
use crate::repository::git::GitRepo as GR;
use crate::repository::docker::ContainerRepo as CR;
use crate::description_defs::{CreationDef, Containers};

/// Parses the creation definition file, and constructs a [CreationDef](struct.CreationDef.html)
pub fn parse_creation_definition(filename: &Path) -> CreationDef {
    assert!(filename.exists(), "Creation definition file does not exist");

    let f = std::fs::File::open(filename).expect("Cannot open file");
    let model: CreationDef = serde_yaml::from_reader(f).expect("Cannot parse yaml");
    model
}

/// Creates the package from the definition file
/// The package will be output into .onboard/package folder of the currently directory
pub fn create_package<AR: ArtifactRepository>(def_path: PathBuf, artifact_repo: &AR, skip_dependencies: bool) {
    // Make sure our file has some anchor so canonicalize will work (it needs a ./ in the front)
    let absolute_file = def_path.absolute_path();
    let root_dir = absolute_file.parent()
        .expect("Could not determine the definition file's parent directory");
    let def = parse_creation_definition(absolute_file.as_path());

    // delete/create the target output directory
    let out_dir_path_buf = root_dir.join(".onboard/package");
    let out_dir = out_dir_path_buf.as_path();
    if out_dir.exists() {
        remove_dir_all(out_dir).expect("Could not remove existing output directory");
    }
    create_dir_all(out_dir).expect("Could not create an output directory");

    let mut effective_creation_def = def.clone();
    // handle the source code exports. We end up with a list of project/repo names inside the package
    // (e.g. the {name}.git value)
    let this_repo = pull_git_repo(&def.repo, out_dir, artifact_repo);
    let mut child_repos = pull_git_repos(&def.child_repos, out_dir, artifact_repo);

    // We are choosing to merge the onboard definition here since we
    // can easily reason about deduping things like containers this way 
    // (vs managing separate packages as children)

    // need to avoid any infinite loops due to cycles
    let top_level_checksum = calculate_creation_yaml_checksum(&absolute_file);

    let mut processed_repos = vec![top_level_checksum];
    let mut i = 0;
    // XXX this merging logic was clearly a bad idea, it used to simplify
    // the logic but has probably lost its way...
    while i < child_repos.len() {
        let repo = child_repos.get(i).expect("Could not retrieve the child repo at the given index");

        // Calculate the target checkout directory, and onboard config location
        let child_onboard_dir = out_dir.join(repo.project.clone());
        let child_onboard_cfg_pbuf = child_onboard_dir.join("onboard.yaml");
        let child_onboard_cfg_path = child_onboard_cfg_pbuf.as_path();

        // if there is an onboard.yaml in the root of the new repository, process that repo as a child repo
        if child_onboard_cfg_path.exists() {

            // calulate the checksum, so we can ensure we haven't processed it yet
            let child_checksum = calculate_creation_yaml_checksum(child_onboard_cfg_path);
            if processed_repos.contains(&child_checksum) {
                println!("Ignoring duplicate repository: {}", child_onboard_cfg_path.to_string_lossy());
                continue;
            }

            println!("Processing child onboard project: {:?}", child_onboard_cfg_path);
            // it is a new one, so add it to the set
            processed_repos.push(child_checksum);

            // parse the child yaml file. We will then manually pull any repos from it,
            // and merge the yaml file into our top level one (effectively flattening the defs)
            let mut child_yaml = parse_creation_definition(child_onboard_cfg_path);

            // The repo URL and branch support @this. We can resolve those
            // references here, and help with merging
            if child_yaml.repo.repo == "@this" {
                child_yaml.repo.repo = repo.origin_url.clone();
            }
            if child_yaml.repo.branch == "@this" {
                child_yaml.repo.branch = repo.branch.clone();
            }

            // The definition for this repository INSIDE the repository is assumed to be the most accurate
            // Not attempting to merge them, just taking their word for it
            let repo_eq = |r: &&mut ManifestGitRepo| r.origin_url == child_yaml.repo.repo && r.branch == child_yaml.repo.branch;
            if let Some(existing) = child_repos.iter_mut().find(repo_eq) {
                existing.helm = child_yaml.repo.helm.clone();
                existing.tests = child_yaml.repo.tests.clone();
            }

            // The other repos are transitive dependencies, go and pull them
            let grandchildren = pull_git_repos(&child_yaml.child_repos, out_dir, artifact_repo);

            for gchild in grandchildren {
                // We will assume the child has a more specific definition, therefore it will take the old one's place
                if let Some(index) = child_repos.iter().position(|r| r.repo == gchild.repo) {
                    child_repos.remove(index);
                }
                child_repos.push(gchild);
            }

            // merge the manifest into this one
            effective_creation_def.merge_child(&child_yaml, &child_onboard_dir);
        }
        i += 1;
    }

    //at this stage we can add our main repo together with the children (the distinction is irrelevant going forward)
    child_repos.insert(0, this_repo);
    create_package_artifacts(effective_creation_def, artifact_repo, child_repos, skip_dependencies, out_dir);
}

fn create_package_artifacts<AR: ArtifactRepository>(def: CreationDef, artifact_repo: &AR, repos: Vec<ManifestGitRepo>, skip_dependencies: bool, out_dir: &Path) {
    // handle the helm charts
    if ! skip_dependencies {
        let helm_discovered_imgs = discover_helm_images(&def, &repos, out_dir);

        // handle the docker containers, we package them up and end up with a list of
        // generated file names containing each containers saved image
        let all_containers: &Vec<&Containers> = &def.containers.iter()
            .chain(helm_discovered_imgs.iter())
            .map(|a| a)
            .collect();

        let container_pkg = artifact_repo.container_repo().package_containers(all_containers, out_dir);
        let container_packages = if let Some(tar) = container_pkg {
            vec![PathBuf::from(tar)]
        } else {
            vec![]
        };

        // XXX these are not the most efficient ways to navigate these definitions, but it is adequate and easy for now
        let all_container_names = all_containers
            .iter()
            .map(|c| c.name.clone())
            .collect();
        create_manifest(def, repos, container_packages, all_container_names);
    } else {
        create_manifest(def, repos, vec![], vec![]);
    }
}

pub fn discover_helm_images(def: &CreationDef, repos: &[ManifestGitRepo], project_dir: &Path) -> Vec<Containers> {
    let mut helm_discovered_imgs: Vec<String> = Vec::new();
    let mut dedup_helm_containers: Vec<Containers> = Vec::new();

    for repo in repos {
        // We are going through the helm charts, to discover which images are 
        // needed for deployment of those charts. We no longer package up
        // the helm charts as tgz. We will reference them from the git repos instead
        for helm in repo.helm.iter() {
            let helm_dir_buf = project_dir.join(repo.project.clone()).join(helm.chart.as_path());
            let helm_dir = helm_dir_buf.as_path();
            println!("Handling helm dir: {:?}", helm_dir);
            let imgs = Helm::package_helm_imgs(helm_dir, &helm.forward_vars, helm.skip_dependency_update);
            helm_discovered_imgs.extend(imgs);
        }

        // merge and dedup the discovered images package from helm and
        // the ones explicitly declared in the docker section
        for c in &helm_discovered_imgs {
            if def.containers.iter().find(|i| i.name == c.as_ref()).is_none() {
                // duplicates can happen within the chart definition
                if dedup_helm_containers.iter().find(|i| i.name == c.as_ref()).is_none() {
                    // TODO I believe we allow failures because we are not checking if we can build the images here
                    // probably some sort of a merge works fine
                    let helm_c = Containers{name: c.clone(), allow_pull_failure: true, dockerfile: None};
                    dedup_helm_containers.push(helm_c);
                }
            }
        }
    }
    dedup_helm_containers
}

// Pull the git repositories and return a list of names where they are saved
// (e.g. the {name}.git value)
pub fn pull_git_repos<AR: ArtifactRepository>(repos: &[GitRepo], out_dir: &Path, artifact_repo: &AR) -> Vec<ManifestGitRepo> {
    // TODO we may end up with duplicates when we do dependencies this way
    // either place them in a subfolder, or append a number?
    repos.iter().map(|r| pull_git_repo(r, out_dir, artifact_repo)).collect()
}

pub fn pull_git_repo<AR: ArtifactRepository>(repo: &GitRepo, out_dir: &Path, artifact_repo: &AR) -> ManifestGitRepo {
    assert!(
        repo.repo_type == "git",
        "Unsupported repo type. One of {{git}} must be used"
    );
    println!("Handling repo: {}", repo.repo);
    let proj_name = artifact_repo.git_repo().package_repo(&repo.repo, &repo.branch, repo.truncate_history, out_dir);
    ManifestGitRepo {
        branch: repo.branch.clone(),
        project: proj_name.clone(),
        repo: PathBuf::from(proj_name),
        origin_url: repo.repo.clone(),
        tests: repo.tests.clone(),
        helm: repo.helm.clone(),
        blacklisted_source: repo.blacklisted_source.clone()
    }
}

/// Constructs the manifest for the package. This manifest is what is read
/// when we get to the ingest phase
fn create_manifest(
    def: CreationDef,
    repos: Vec<ManifestGitRepo>,
    container_files: Vec<PathBuf>,
    container_names: Vec<String>,
) -> PathBuf {
    let manifest_file = PathBuf::from(".onboard/package/manifest.yaml");
    let manifest = Manifest {
        name: def.name,
        version: def.version,
        git: Some(repos),
        containers: Some(DockerContainers {
            load_files: Some(container_files),
            container_names
        }),
    };

    // output the manifest to the file
    let file_handle = File::create(&manifest_file).expect("Could not open output manifest file");
    serde_yaml::to_writer(file_handle, &manifest).expect("Could not write to output manifest file");

    manifest_file
}

impl CreationDef {
    fn merge_child(&mut self, other: &CreationDef, base_directory: &PathBuf) {
        // XXX there are cleaner ways to transform these, just a big mutable transform probably
        // Probably just wrap the structure in a subchart that maintains where it was checked out. merging got awkward quick

        // We need to rewrite the child paths. They were defined as relative, and we aren't tagging them with any subchart
        let transformed = other.containers.iter()
            .map(|a| {
                // replace the dockerfile with the newly rewritten one, if there is one defined
                match &a.dockerfile {
                    Some(df) => {
                        Containers {
                            dockerfile: Some(format!("%{}/{}", base_directory.to_owned_str(), df)),
                            ..a.clone()
                        }
                    },
                    None => a.clone()
                }
            });

        // we will end up with child_repos, and we currently don't have any
        let mut our_children = vec![];

        self.containers.extend(transformed);
        our_children.append(&mut other.child_repos.clone());

        // Check the repo that 
        // TODO i'm pretty sure there are conflict issues if charts depend on the same repos (but they get namespaced in git...think about it)
        if let Some(r) = our_children.iter_mut().find(|r| r.repo == other.repo.repo) {
            // XXX probably more future safe to remove the item from the list and plop in the new one
            r.helm.append(&mut other.repo.helm.clone());
            r.tests.append(&mut other.repo.tests.clone());
        } else {
            our_children.append(&mut other.child_repos.clone());
        }

        self.child_repos.append(&mut our_children);

        println!("Result: {:?}", self);
        println!();
    }
}

fn calculate_creation_yaml_checksum(filename: &Path) -> String{
    let mut f = std::fs::File::open(filename).expect("Cannot open file");
    let mut buf = Vec::new();
    f.read_to_end(&mut buf).expect("Failure reading the onboard config file while trying to calculate a checksum");
    let mut checksum = sha1::Sha1::new();
    checksum.update(&buf);
    checksum.digest().to_string()
}


#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    pub fn test_merge_repos() {
        let mut incoming = CreationDef {
            name: "c1".to_owned(),
            version: "1".to_owned(),
            containers: vec![Containers {..Default::default()}],
            repo: GitRepo{repo: "r1".to_owned(), ..Default::default()},
            child_repos: vec![GitRepo{..Default::default()}],
        };
        let mut merger = CreationDef {
            name: "c2".to_owned(),
            version: "2".to_owned(),
            containers: vec![Containers {..Default::default()}],
            repo: GitRepo{..Default::default()},
            child_repos: vec![GitRepo{..Default::default()}],
        };
        incoming.merge_child(&mut merger, &PathBuf::from(".")); 
        assert_eq!(incoming.name, "c1");
        assert_eq!(incoming.version, "1");
        assert_eq!(incoming.containers.len(), 2);
        assert_eq!(incoming.repo.repo, "r1");
        assert_eq!(incoming.child_repos.len(), 2);
    }
}