use std::path::{PathBuf};

pub trait PathExtensions {
    fn absolute_path(&self) -> PathBuf;
    fn to_owned_str(&self) -> String;
}

impl PathExtensions for PathBuf {
    fn absolute_path(&self) -> PathBuf {
        // Make sure our file has some anchor so canonicalize will work (it needs a ./ in the front)
        let absolute_file = if self.is_relative() {
            PathBuf::from("./").join(self)
        } else {
            // These are root, relative, parent, or other prefix absolute types
            self.to_path_buf()
        };
        println!("{:?}", absolute_file);
        absolute_file.canonicalize()
            .unwrap_or_else(|_| panic!("Could not create absolute path from : {}", absolute_file.to_string_lossy()))
    }

    fn to_owned_str(&self) -> String {
        String::from(self.to_string_lossy())
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_to_owned_str() {
        assert_eq!(String::from("/some/absolute.ext"), PathBuf::from("/some/absolute.ext").to_owned_str());
    }

// XXX these tests are not portable (or good)
//    #[test]
//    fn test_absolute_path() {
//        assert_eq!(PathBuf::from("/some/absolute/file.ext"), PathBuf::from("/some/absolute/file.ext").absolute_path());
//    }
//
//    #[test]
//    fn test_relative_to_absolute() {
//        let path_str ="testFile";
//        let relative_path = PathBuf::from(path_str);
//        let ending = String::from("/") + path_str;
//        // canonical will fail if the file doesn't exist
//        File::create(&relative_path).expect("Could not create file");
//        let absolute_path = relative_path.absolute_path();
//        assert!(absolute_path.is_absolute());
//        assert!(absolute_path.ends_with(ending.as_ref() as &str));
//        remove_file(relative_path).expect("could not delete file");
//    }
}