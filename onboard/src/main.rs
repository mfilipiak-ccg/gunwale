//! Onboard is a helper utility focused on packaging projects for airgap deployment, as well
//! as facilitating automation of integrated projects via a pipeline and k8s deployment.
//! 
//! ## Quickstart
//! 
//! ```sh
//! dev:~/project> onboard create onboard.yaml
//! dev:~/project> tar cJf project-delivery-1.0.0.tar.xz .onboard/package
//! dev:~/project> scp project-delivery.tar.xz staging:/landing/project
//! 
//! # on a server inside the target environment
//! staging:/landing/project> tar xJf project-delivery-1.0.0.tar.xz
//! staging:/landing/project> onboard load -c ~/onboard_load_config.yaml .
//! ```
//! 
//! ## Typical Process
//! 
//! 1) Create an [onboard.yaml](description_defs/struct.CreationDef.html) to describe the project
//! 2) Run `onboard create`
//! 3) Deliver package
//! 4) Run `onboard load`
//! 5) Create a master integration project, using submodules 
//!    ([See Palomar Project](https://gitlab.com/mfilipiak-ccg/palomar))
//! 6) Use `onboard generate` inside CI server to create jobs for each project
//!    ([See Palomar CI file](https://gitlab.com/mfilipiak-ccg/palomar/-/blob/master/.gitlab-ci.yml))
//! 
//! ## Assumptions for environment being delivered into:
//! 
//! 1) A docker registry is available and setup to push to
//! 2) A GitLab respository
//! 3) GitLab API tokens able to read/write projects 
//! 4) Deprecated - A MongoDB is available
//! 5) An [onboard config](config/index.html) pointing to these resources
//! 
//! ## Deprecated Functionality
//! 
//! This tool also supports pushing deploy tags into a MongoDB database. Initially that
//! was a self contained way to track all artifacts and builds within a pipeline. That behavior is
//! now deprecated in favor of using Git as the main source of truth for dependencies. Use the
//! `use_tagging` flag to enable that behavior.
//! 
#[macro_use]
extern crate serde_derive;
extern crate flate2;
extern crate mongodb;
extern crate serde_json;
extern crate serde_yaml;
extern crate tar;
extern crate sha1;
#[macro_use]
extern crate lazy_static;

use std::io::{Error, ErrorKind};
use std::path::{PathBuf};
use structopt::clap::Shell;
use structopt::StructOpt;
use std::panic;

mod shell_runner;
mod creation;
mod config;
mod description_defs;
mod helm;
mod job_generation;
mod normalize_registries;
mod package_ingest;
mod path_extensions;
mod query;
mod repository;
mod tagging;

use repository::{GitlabDockerRepository};

/// The options available to the CLI
#[derive(StructOpt, Debug)]
#[structopt(about = "Airgap compatible artifact creator & loader")]
enum Onboard {
    /// Loads an artifact into an environment
    Load {
        /// File paths to the packages to process into the system. Packages
        /// must conform to the onboard file format. See documentation for help.
        #[structopt(name = "FILE", parse(from_os_str))]
        files: Vec<PathBuf>,

        #[structopt(short="c", long="config")]
        config_file: PathBuf,

        /// Skip tagging during ingest. This should really only
        /// be used when deploying the artifacts used to do the
        /// tracking
        #[structopt(long="use-tagging")]
        use_tagging: bool,
    },
    /// Creates a package based on a definition that can be used for deployment
    Create {
        /// Create a package from a onboard creation definition file
        #[structopt(name = "FILE", parse(from_os_str))]
        file: PathBuf,

        /// Creates a lightweight package. Useful for iterating tests,
        /// where the dependencies are known to be loaded. Not suitable
        /// for cross environment deploying
        #[structopt(long="skip-dependencies")]
        skip_dependencies: bool,
    },
    /// Utility that converts docker image names inside of a k8s manifest.
    /// Works off of stdin (e.g. pipe manifest, or route through helm --post-renderer)
    Normalize {
        /// Registry name (e.g. "my.docker:5000")
        #[structopt(short="n", long="name")]
        name: String,
        /// In default mode, the normalization happens on
        /// a helm chart, therefore matching uses "image:". In this
        /// mode, it will simply match each line
        #[structopt(short="s", long="simple-match")]
        simple_match: bool
    },
    /// Queries for tagged information when using a tagging repo (Deprecated as of 1.2 in favor
    /// or GitLab workflows)
    Query {
        #[structopt(short="c", long="config")]
        config_file: PathBuf,

        #[structopt(short="t", long="type")]
        query_type: String,

        #[structopt(short="i", long="id")]
        package_id: String
    },
    /// Generates GitLab style CI files, intended for use by GitLab CI trigged child jobs
    GenerateJobs {
        /// The directory that houses the git repository
        #[structopt(name = "FILE", parse(from_os_str))]
        directory: PathBuf,

        /// The target output directory. 
        #[structopt(long="output-dir", short="o")]
        output_directory: PathBuf
    },
    /// Creates completion files for a given shell.
    /// Example: `onboard completion bash /etc/bash_completion.d/`
    Completion {
        /// The shell to generate completions for (e.g. 'bash').
        /// Supported shells are those which clap supports
        shell: Shell,

        /// The output directory for the completion file
        #[structopt(name = "FILE", parse(from_os_str))]
        directory: PathBuf,

    }
} 

fn main() -> Result<(), Error> {
    match Onboard::from_args() {
        Onboard::Load {files, config_file, use_tagging} => {
            if files.is_empty() {
                return Err(Error::new(ErrorKind::InvalidInput, "You must specify at least one file to load"));
            }
            let repo = GitlabDockerRepository::new(Some(config::load_config_file(config_file)));

            // supports multiple packages, which is natural from the terminal (expansion, etc)
            for file in files {
                package_ingest::ingest_package(file.as_path(), &repo, use_tagging);
            }
            Ok(())
        },
        Onboard::Create {file, skip_dependencies} => {
            assert!(
                file.exists(),
                "Must supply a valid definition file to create a package!"
            );
            let repo = GitlabDockerRepository::new(None);
            creation::create_package(file, &repo, skip_dependencies);
            Ok(())
        },
        Onboard::Normalize {name, simple_match} => {
            // XXX this is meant as a repository of the ingest, but our traits are too broad right now, so sharing them
            let repo = GitlabDockerRepository::new(None);
            normalize_registries::normalize_from_stdin(&repo, &name, simple_match);
            Ok(())
        },
        Onboard::Query {config_file, query_type, package_id} => {
            let repo = GitlabDockerRepository::new(Some(config::load_config_file(config_file)));
            query::query_for(repo, package_id.as_ref(), query_type.as_ref())
        },
        Onboard::GenerateJobs {directory, output_directory} => {
            let repo = GitlabDockerRepository::new(None);
            job_generation::create_job_definitions(&directory, &output_directory, &repo);
            Ok(())
        },
        Onboard::Completion {shell, directory} => {
            Onboard::clap().gen_completions(env!("CARGO_PKG_NAME"), shell, directory);
            Ok(())
        }
    }
}