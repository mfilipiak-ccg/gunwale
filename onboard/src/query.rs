use std::io::{Error, ErrorKind};
use crate::repository::ArtifactRepository;
use crate::tagging;

pub fn query_for<AR: ArtifactRepository>(
    repo: AR,
    package_id: &str,
    query_type: &str,
) -> Result<(), Error> {
    let conn = tagging::create_connection(repo.config());
    // find out what type of thing we're trying to retrieve
    match query_type {
        "containers" | "docker" => {
            let containers = tagging::query_docker_containers(package_id, conn)?;
            for c in containers {
                println!("{}", c);
            }
            Ok(())
        },
        "helm" => {
            let charts = tagging::query_helm_charts(package_id, conn)?;
            for c in charts {
                println!("{} {}", c.proj_name, &*c.chart_location.to_string_lossy());
            }
            Ok(())
        },
        "tests" | "test" => {
            let repos = tagging::query_repos(package_id, conn)?;
            for r in repos {
                for t in &r.tests {
                    // TODO this is not currently where you put the repo
                    println!("{} {}", r.project, t.to_string_lossy().to_owned());
                }
            }
            Ok(())
        },
        "source" | "git" => Ok(()),
        _ => {
            let msg = format!("Unknown query type: {}", query_type);
            Err(Error::new(ErrorKind::InvalidInput, msg))
        },
    }
}
