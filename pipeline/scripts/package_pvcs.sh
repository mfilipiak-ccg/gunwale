#/bin/bash

set -e

SCRIPT_DIR=$(readlink -f $(dirname $0))
ROOT_DIR=$SCRIPT_DIR/../..

# Avoiding using the script_dir here so our tar will be relative without other adjustments
BUILD_DIR=build
ARTIFACT_DIR=$SCRIPT_DIR/artifacts

rm -rf $BUILD_DIR $ARTIFACT_DIR > /dev/null 2>&1 ||:
mkdir -p $BUILD_DIR
mkdir -p $ARTIFACT_DIR

# This includes some jenkins stuff as well just in case, but is here for the anchore definitions
mkdir -p $BUILD_DIR/pipeline_postgres
echo "This gets loaded into /var/lib/postgresql/data (in the future via an init-container" > $BUILD_DIR/pipeline_postgres/README
POSTGRES_POD=$(kubectl -n pipeline get pods --selector=app=postgresql -o jsonpath="{.items[0].metadata.name}")
kubectl -n pipeline cp $POSTGRES_POD:/var/lib/postgresql/data $BUILD_DIR/pipeline_postgres

# grab Jenkins plugins
mkdir -p $BUILD_DIR/jenkins_plugins
JENKINS_POD=$(kubectl -n pipeline get pods --selector=app.kubernetes.io/name=jenkins -o jsonpath="{.items[0].metadata.name}")
echo "This gets loaded into /var/jenkins_home/plugins (in the future via an init-container" > $BUILD_DIR/jenkins_plugins/README
kubectl -n pipeline cp $JENKINS_POD:/var/jenkins_home/plugins $BUILD_DIR/jenkins_plugins

# compress everything into a package
# jenkins artifacts
# TODO we should populate a container and use it as an init-container to autopopulate the PVC
echo "Compressing artifacts into gunwale_pvc_data.tar.xz"
XZ_OPT=-9 tar cJf $ARTIFACT_DIR/gunwale_pvc_data.tar.xz $BUILD_DIR/jenkins_plugins

# XXX set trap for removing build_dir
#rm -rf $BUILD_DIR > /dev/null 2>&1 ||: