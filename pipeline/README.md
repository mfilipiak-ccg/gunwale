# DevSecOps Pipeline Deploy

The goal is to autodeploy and update a pipeline for compliance validations, deployment, and test.

There are a two major flavors that have beeen made into proofs of concept. One that leverages Jenkins, and the other using GitLab. Jenkins was initially investigated heavily to be in line with an external groups solution. They have started to shy away from that recommendation, which opens up the more natural GitLab based workflows.

Refer to the [Delivery](../documentation/delivery.md) section to get more information on how to update projects in an integrated setting.

The recommended workflow for integration is outlined by the [Palomar](../examples/palomar/README.md) project. It is a Gitlab based setup that uses [onboard](../onboard/README.md) for both delivery, project description, as well as CI job generation.

## Stages

Both solution (or any CI technology solution) aim to deploy from scratch a DevSecOps CI server. By deploying those CI servers into the cluster (or at least the pipeline slaves/runner), some interactions with the cluster is simplified. Most notably, during testing we can talk to services without needing a specific ingress defined.

The DevSecOps pipeline stages generally include stages for:

* Code Quality/Compliance Validation
* Deployment
* Testing

## Onboard's role

Onboard is the tool being used to drive both pipelines. It is not a requirement, and can easily be replaced with another set of scripts. You may choose to use a portion of the tool, and replace the rest as well.

### Project Description

The most important piece from the perspective of a CI platform is a way to describe a project's deliverables and layout. It is typical of a repository to contain many scripts and tools that are in support of the deliverable features, but not part of the deployed 

The approach illustrated by this project uses a YAML file to declare the deployed portions of the project. 

Additionaly, there are auto-discover capabilities in the tool. Although this is intended to avoid missing packaging components during delivery, it can also be used to validate that the declared manifest matches what is actually getting deployed. However, this is largely left as a todo in the project. 

The key is that the declarations are tracked in CM for the project, so it is auditable what items are subject to interrogation through the pipeline. Exclusion from the pipeline is 'blacklisted', which provides a clear view of what was excluded. A justification for each blacklisted item should be provided.

### Packaging

This pipeline assumes it will be located in an isolated network, and an internet connection is not assumed. Onboard assists in making a package that includes all necessary artifacts to support this delivery.

* Packages source code
    * Supports remote support repositories
    * First class support for helm
    * Includes test projects
* Packages docker images
    * Support auto-discovery via helm charts

### CI Execution

Onboard helps to generate files based to help drive the CI pipeline. This is really just a proof of concept, and an easy place to place the scripts because of an intimate knowledge of the structure.

Ultimately, the pipeline needs to minimally know the following details:

* Where the source code is located (for static code analysis)
* Which docker images are used in the deployment (for container scanning)
* Where the install scripts are (for deployment)
* Where the tests are located, and how to run them (for integration testing)

Jenkins
: In the initial cuts of the CI server, we were using a MongoDB as the central tracking system. This is now deprecated, given having a strong CM via Git is simplier and sufficient. In the earlier stages, there were many more repositories being pushed to that justified the separate database (e.g. helm). Even though the example scripts in the Jenkins project utilize this database, it could adopt a similar flow to the Gitlab pipeline.

Gitlab
: The onboard utility will generate .gitlab-ci.yml scripts that can be triggered through a master pipeline.

Note: Onboard is limited in the fact that it only provides special tooling for python projects (i.e. it pylints .py files). It also only supports deploying via helm (thus assumes a k8s deployment for all projects)

## Dependency Services

* [Clair - Docker Scanner](./clair-chart/README.md)
* [GitLab](./gitlab-chart/README.md)
* [Jenkins Pipeline, Anchore](./gitlab-chart/README.md) [ *Anchore should be split up, or Jenkins removed* ]


## Dockerfiles

There are a set of dockerfiles that are referenced in the the CI scripts. If moved out of prototype, it would make sense to build those and make them available via this project.

[Dockerfiles for CI scripts](./ci/dockerfiles)

## Scripts

Certain services populate dependencies or other data after boot. This poses a problem when packaging for offline deployment. One strategy is to deploy the service, wait a period of time to make sure everything gets downloaded, and then copy the data out of the PVCs. A helper script is used to perform that action.

[PVC package script](./scripts/package_pvcs.sh)

There are two such services in this project. Jenkins will fetch the plugins defined in the [JCasC](./pipeline-chart/values.yaml) section of the deploy configuration.

The second is the Anchore tool, which needs to download the most recent stream of CVEs. It appears them support using a 