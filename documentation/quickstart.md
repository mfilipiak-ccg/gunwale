# Gunwale Quickstart

If you are just getting started, and want to use what is provided inside this project, this is a good place to start. There are several ways to accomplish the same goals within this domain (and within this project). This page offers one recommended path to exploring the project and getting started.

1. Familiarize yourself with the basics of kubernetes (https://kubernetes.io/docs/home/)
1. Deploy a Kubernetes cluster (See [Offline](../deployment/offline/README.md) or [k8s Cluster](./k8s_clusters.md) sections)
1. Install Istio into you cluster
1. Explore [Palomar](../examples/palomar/README.md) for examples of deploying a simple microservice into a cluster, utilizing Istio
1. Deploy GitLab, and utilize the example pipeline
1. Consider a master project to CM all integrated components (see [Palomar](../examples/palomar/README.md))
1. Package artifacts for delivery using [onboard](../onboard/README.md)
1. Utilize a common set of configs to drive a pipeline from validation to deployment to test (see [Palomar's Gitlab CI](../examples/palomar/.gitlab-ci.yml))