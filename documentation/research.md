# CH Architecture & Pipeline Research

## Key Goals

* Unintrusive integration
* Built upon modern architecure
* Avoid tying integrations into specific technologies
* Agnostic to deployment targets
  * May be deployed in the cloud and on-prem for testing
* Supports scalable microservice based architecture
* Deliverable (wholely or partially) to remote sites for on-prem installation for testing

## Architecture

### AWS

Targeting AWS as a platform is a natural choice. Many problems are simply 'solved' on the AWS platform (and many solutions are added regularly).

The issue with building on top of AWS supplied technologies, is you become very tied into AWS. This is a problem considering how some development teams are performing their work. A connection to AWS should not be assumed to test integration. However, we do want to be deployable on AWS if available, and take many of the lessons learned from their most popular systems.

#### EKS (K8S)

This is an out of the box managed kubernetes cluster. It saves the installation and configuration of the cluster. It costs more than if you used EC2 to deploy a cluster yourself.

#### IAM (Authentication/Authorization)

Fantastic access control design. Very rich declarative resource specific configuration. There is a learning curve to this model, but the value added is worth it. It is a model that can be drawn from extensively if needed.

#### KMS & Secrets Manager (Key & Password Management)

Centralized key and password management.

#### CloudWatch

Logs and event monitoring for AWS. Alarms can be paired with auto-scaling groups to trigger resource quantity increase or decrease based on infrastructure load demands.

#### Lambda (serverless)

Provides serverless functions.  Many languages are supported, and are simple to write and maintain. Interesting to tie into certain APIs or events (e.g. cloudwatch). Today's architecture does not need this, but if it were available, some microservices may simply live as a lambda function

#### EC2

Fairly familiar technology prior to research

* Explored networking/firewalling/IAM controls
* Explored more involved networking/firewalling/VPC peering/IAM controls
* Discovered AWS regions are connected via dedicated private 100 GbE fiber
    * https://aws.amazon.com/about-aws/global-infrastructure/global_network/
* Explored EFS - HA shared filesystem capable of backing multiple nodes
* Explored asynchronous decoupling with SQS (Simple Queue Service)
* Explored ElastiCache (managed Redis or Memcached)
* Explored DynamoDB (NoSQL database)
    
#### CloudFormation

Declarative resource/service provisioner. Similar functions to Terraform. Offers deep integration into AWS than terraform, especially through state management with Stacks. A Stack is a collection of resources that can be managed as a single unit. Well implemented and thought out, but Terraform is preferred due to multi-cloud provisioning abilities.

#### VPC

Offers high levels of control for traffic flows between different resources without needing to touch public networks. Can be peered together to selectively allow traffic to pass between different groups of resources.

### Kubernetes (k8s)

When you look at the needs of the architecture, a few things stand out.

* There are many features that an infrastructure should provide (described below)
* Projects/Services generally should focus on creating docker containers containing the implementations
* Deployments into the infrastructure should be standardized

Kubernetes is a nice fit to deliver these capabilites of a platform. It is the de-facto industry standard microservices orchestration platform. Major cloud providers (such as AWS and Azure) have integrated Kubernetes into their platforms so that it becomes an abstraction layer for the cloud itself. For example, provisioning a Kubernetes Load Balancer Service in a cluster on AWS or Azure automatically provisions a Load Balancer resource on the cloud infrastructure. This abstraction means developers have a common deployment target - it doesn't matter whether the application will eventually run on premises in a server room or on cloud infrastructure. Kubernetes is also modular and allows for integration of external components, such as VMWare's NSX-T or Cisco's ACI to better manage Kubernetes networking. Finally, Kubernetes has a well documented API that allows for beautiful integration into tools like GitLab CI for testing/rolling releases.

Helm is the preferred tool for kubernetes to package releases. You can create dependencies to compose multiple pieces are the infrastructure into a single unit. In this model, each component/service being installed on the architecture would provide a helm chart and docker images into an artifact repository. Centralized configuration, key management, authentication/authorization systems, and service discovery will simplify tying all the disparate pieces today.

#### OpenShift/OKD

Red Hat's OpenShift (OKD is the opensource upstream) eases the deployment of the kubernetes cluster. It supports the vast majority of kubernetes features (typically lags). It also adds some additional features onto kubernetes, if you are willing to sacrifice using vanilla kubernetes as a deployment option. Cloud providers offer Red Hat Open Shift as a deployment option for an extra cost, though the ease of use and integrations of this approach are currently unknown.

OpenShift is currently going through a time of major transition with Red Hat's purchase of CoreOS. CoreOS was the creator of a feature rich on-premises kubernetes platform called Tectonic and early innovators in the container-native infrastructure space. We are very interested to see how RedHat plans to offer the tools built by CoreOS, as they offered a solution that was far closer to vanilla Kubernetes than OpenShift.

#### Kubernetes vs AWS

This is not a direct comparison, as k8s is compatible on AWS. This comparison is intended to compare features available ONLY in AWS to alternatives that can be used in an offline manner.

| AWS only           | Offline Options
| -------------------|-----------------------------
| EKS                | k8s, Minikube, OpenShift
| Autoscaling        | k8s autoscaling
| VPC                | k8s network policies, external network components
| Lambda             | kubeless.io
| Secrets manager    | Vault (Hashicorp), k8s Secrets
| KMS                | Vault (Hashicorp), k8s Secrets
| IAM                | Vault (Hashicorp), k8s Service Accounts
| SQS/Kinesis        | AMQP (e.g RabbitMQ)

Some options, such as vault, can swap backends to utilize AWS solutions when available.

```mermaid
  graph LR
     actor["fa:fa-male" Actor]
     subgraph kubernetes
        IngressProxy
        subgraph pod1
           sidecar1[Envoy Sidecar] 
        end
        IngressProxy --> sidecar1
        sidecar1 -- ACL --> Vault
        sidecar1 -- Metrics --> Prometheus
        sidecar1 -- Tracing --> Jaeger
        sidecar1 -- Logs --> Fluentd
     end
     actor -- HTTP --> IngressProxy
     actor -- JWT --> Vault
```

### Infrastructure Features

* Service discovery
* Monitoring
  * Prometheus, Nagios, Logging, etc
* Observability
  * Service Mesh (e.g. Istio, Consul)
  * Tracing, Health
* Key Management
  * Vault 
  * KMS
* Authentication
  * Vault
  * IAM, Secrets Manager
* Authorization
  * JWT
  * IAM

## Continuous Integration

### Key Goals

* Automated deployment from CI servers
* Simplified/uniform deployment into infrastructure for like components
* Environment specific configuration available
  * Necessary, but certainly the rare case, and discouraged
* Supports packaging for deployment into non-connected production environements

### Software

* Artifactory
  * Docker, Helm, RPM, etc
  * Rich API for custom tagging and cleanup
* Gitlab
  * Continuous Integration
  * Configuration management
* Kubernetes/Helm
  * Deployment of Docker artifacts

```mermaid
graph LR
    subgraph dev
    DevPipeline --> Scan
    DevPipeline --> Deploy
    DevPipeline --> Test
    Test --> Artifacts
    end

    Artifacts == Docker/Helm ==> ProductionArtifactRepo

    subgraph integration
    ProductionArtifactRepo -- Trigger --> IntPipeline
    IntPipeline --> IntScan
    IntPipeline --> IntDeploy
    IntPipeline --> IntTest
    end

    IntTest -- Tag --> ProductionArtifactRepo
```