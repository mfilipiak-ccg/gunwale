# Project Delivery

When development occurs in small independent teams (or separate companies), the delivery into a unified system can be challenging. The strategy outlined below aims at meeting all of these goals. Tooling is an important part of all modern development. It is important to allow teams to develop and deliver in a way that matches their style, while providing a natural integration experience.

* Proper CM and version management occurs
  * Must always be able to tell what is on the system at any given time
* Simplified packaging for offline deployments
* Hands off approach to compliance and code quality scanners
* While conventions ease integration, impose conventions in a way that allow the project to leverage methods and code they are utilizing already
  * Think indirection, rather than imposing development processes
* Where possible, describe the project, rather the way in which they execute

## Approach

This approach uses a yaml file inside of each project. This is used to make sure that the project and all dependencies are packaged up, and the CI server knows how to handle the project. It is acknowledged that all pieces of a repository are not meant for production, so we can use this to concept to allow us to describe what is relevent for deployment, and should be validated as such.

### Deployment

Every component must be capable of deploying in an automated watch from scratch. Each commit in the repository references a point that is able to be installed on a fresh system.

The CI server should have native support for a set of deployment strategies. The deployments may depend on environment variable, but should otherwise be as deterministic as possible.

In this project, onboard currently only supports helm as a deployment mechanism. It is expected that ansible would be offered as well to support non-kubernetes workflows.

### Testing

The descriptions in onboard can be extended to support non-gitlab style test definitions as well. The early prototypes had targetted Jenkins specifically. What resulted was a definition inside of onboard that very closely resembled the .gitlab-ci.yaml format anyway. This is not surprising since both are in YAML, and the primitives for running a test are basically: docker image, command, environment variables.

Therefore, even though the current definition file is a GitLab specific file, it is intended to support multiple CI servers though a well known definition file. It may be necessary to support a subset of this format, which can be enforced at package time. For example, we have explored parsing this file in a Jenkins Kubernetes pipeline definition, without much issue.

## Alternatives

There are many alternatives to the outlined strategy as well. Some are discussed here, along with the reasons they may not be ideal according to our goals.

#### GitLab's auto-devops Approach

GitLab's auto-devops appears to be tied into heroku definitions. This may be a reasonable approach as well. We have not explored those definition, but like the concept of discovery for source and tests. More exploration is probably necessary in this area, as it is more well known than a new definition file we would be imposing.

#### Project Integration

This is fairly common approach where an integration team or the project modifies an existing set of integration scripts to ensure the new project is validated, deployed, and tested. This typically involves adding or importing chunks of integration scripts at every level of the pipeline to reference the project.

The downside to this approach is that each project ends up with a significantly different strategy, and it is difficult to reason about. It becomes challenging to tell the story about how all things are accomplished. It is also difficult to gather clear direction on the best way to integrate, since there are infinite ways to accomplish the same thing.

#### Common CI scripts per project

This strategy treats each project independently. They offer up templates or scaffolding for each project to base their pipeline off of. There is a lot of flexibility here, but also the potential for drift. Maintenance and propagation of updated scripts becomes an issue as well.
