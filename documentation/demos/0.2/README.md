---
presentation:
  theme: "league.css"
  center: false
---

<!-- slide -->
## Kubernetes

Start with kubernetes and helm, so we can understand the pipeline deployment

* Scaling
* Gateways/Ingress/Services
* Sidecars (High Level)
* Secret / Key management
* Policy Enforcement

<!-- slide -->
## Helm

* Deploy

<!-- slide -->
# Pipeline

* Deploy from scratch
* Jcasc
* Plugin

<!-- slide -->
# Runtime

<!-- slide -->
## Kiali

login: admin/admin (k8s secret capable)
* View core-ui's outbound calls
* Note ingress gateway needs to be istio to get a good push to these tools

<!-- slide -->
## Deployment

* Helm
* Namespaces

<!-- slide -->
## Istio Setup

* Sidecars
* Shippers
* OPA

<!-- slide -->
## Monitoring

* ELK
  * kubernetes.namespace_name: gunwale
* Fluent-bit
  * Node level daemon-set
  * By default will ship all logs from stdout/stderr from containers
* Jaeger/Zipkin
  * Service level tracing without instrumentation
  * EXCEPT: we need to forward headers for best fidelity

<!-- slide -->
## Takeaways

* Questions about deployments during test (dedicated environment, minikube, isolation?)
* What things should be added to existing pipelines
  * What scans/validations on the local env will benefit greatly when adding this lab env
* How to take the features and add capability (without new integration needs)
  * Likely not going to instrument existing code, forwarding headers etc
  * It is a focus (envoy, etc), but some features have some impacts