# Cheap Quick Kubernetes Development Cluster Using k3d/k3s

This is the supporting script for the demo at:  
https://youtu.be/jUPL4ZOlJ0E


## Installation

Everything is running on a fresh Mint VM.

```sh
# k3d will run inside of docker
> apt-get install -y docker.io

# kubectl to interface with your cluster
> curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
> chmod 755 /usr/local/bin/kubectl

# helm for deployment
> curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# kubefwd (the video mistakenly grabs the tar.gz at first)
> wget https://github.com/txn2/kubefwd/releases/download/1.11.0/kubefwd_amd64.deb
> dpkg -i kubefwd_amd64.deb

# download and install k3d
> curl -s https://raw.githubusercontent.com/rancher/k3d/master/install.sh | bash
```

# Launch k3d Cluster

```sh
# Create cluster
> k3d create

# Wire kubectl to use the cluster. You can copy the output of 
# Alternatively, you can copy the out `k3d get-kubeconfig --name='k3s-default` to ~/.kube/config
> export KUBECONFIG="$(k3d get-kubeconfig --name='k3s-default')"

# validate config
> kubectl get namespaces
```

# Example: Deploy Jenkins into the cluster

```sh
> helm repo add codecentric https://codecentric.github.io/helm-charts
> helm install codecentric/jenkins --version 1.5.0 --generate-name
```

# Example: Deploy Custom Application

```sh
# Deploy example service using public docker image
> kubectl create deployment server1 --image=gcr.io/google-samples/hello-app:1.0
> kubectl expose deployment server1 --port 8080
> kubefwd svc
> curl http://server1:8080

# Now use a locally built image
> kubectl create deployment server2 --image=localbuild:1.0
> docker pull gcr.io/google-samples/hello-app:1.0
> docker tag gcr.io/google-samples/hello-app:1.0 localbuild:1.0

# Push the localbuild from our docker daemon into k3d/k3s
> k3d import-images localbuild:1.0

# Create a service for our new deployment 
> kubectl expose deployment server2 --port 8080
> kubefwd svc
```