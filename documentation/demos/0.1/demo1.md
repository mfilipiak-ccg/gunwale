---
presentation:
  theme: "league.css"
  center: false
---

<!-- slide -->
# Gunwale
### Technology Stack
8/19

https://youtu.be/Wgb0HOFb7Mo


<!-- slide -->
## Kubernetes (k8s)

* Lightweight, container based deployments
* Namespaces to segregate deployments
* Rolling upgrades/rollbacks
* Translates to all major cloud architectures

<!-- slide -->
## Gitlab
* The integration with kubernetes is not as seemless as expected
* End up treating it as a "command pipeline"
    * Most systems will support this well
* Natural docker runner support
* Has major benefits for development workflows
    * Merge requests tied into validation/testing
    * k8s environment deploy for review applications
* Many auto-devsecops are paid features

<!-- slide -->
## CI/CD Software
* Recommendation to use Jenkins for devsecops because of plugins
* Likely smart to incorporate tooling via CLI commands
    * Avoids tying to features of CI platform, and typically isn't any more challenging to do
* Need to investigate Jenkins plugins further

<!-- slide -->
## Helm Deployment
* Standard package manager for kubernetes
* Helm 2.x (Helm 3 is in alpha)
* Supports dependency management and configuration
* Allows Chart Developers to expose configuration options

<!-- slide -->
## Service Mesh
* Envoy for sidecar integration (via consul)
* Simple service discovery
* Reasonable Security via ACLs
  * Do not need to worry about key distribution

<!-- slide -->
* Requirements:
    * Service Mesh
    * Secrets Storage



<!-- slide -->
```console
> helm deploy --name gunwale gunwale/ --set DEPLOYMENT_HASH=$HS
```

![](k8s_pod_deployment.png)

<!-- slide -->
* Yellow Highlight:: Application deployment
* Circled:: Application External Proxy (Available outside the cluster)

<p>
<img src="k8s_service_deploy.png" alt="drawing" style="width:500px"/>
</p>

<!-- slide -->
* Internal Services are bound to localhost
* Sidecar ACLs and Consul will provide external connections

<div>
<img src="service_1.png" alt="drawing" style="width:500px; border: none; position: relative; left:-6em"/>
<img src="service_2.png" alt="drawing" style="width:600px; position:relative; left: 4em; top: -7em"/>
</div>

<!-- slide -->
* Gitlab Deployments (expandable)

<img src="gitlab-deploy.png" style="width: 600px"/>

<!-- slide -->
# Future

* Pod/Cluster Security
* Scanning (paid versions of GitLab)
* Artifactory integration