# Demos

These are prepared demos or discussion presentations during the course of the research. Opinions may have changed over time, and these demos will likely not be updated. Please consider the dates that they were posted.

[0.1 - Early Overview](./0.1/demo1.md)
: This is an early overview of kubernetes and technologies supporting it. 
https://youtu.be/Wgb0HOFb7Mo

[0.2 - Kubernetes services](./0.2/README.md)
: Slides detailing a few items inside of a kubernetes cluster. Overview of some monitoring, telemetry, and service mesh technologies. Video discussed using Jenkins Configuration as Code (JCasC) to deploy from scratch pipelines inside of kubernetes.
https://youtu.be/N7WtyjELdiw


[k3d Demo](./k3d_demo.md)
: Shows how to use k3d for a cheap development or IoT kubernetes deployment.
https://youtu.be/jUPL4ZOlJ0E

[Istio Demo](../../examples/palomar/demos/istio-intro.md)
: Shows using Istio for Zero-trust Authorization  
https://youtu.be/-h9NzJqzWeY
