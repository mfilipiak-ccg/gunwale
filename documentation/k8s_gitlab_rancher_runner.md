# Link Gitlab to external Rancher Cluster

Note: This is based on working from the `Kubeconfig File` from the Rancher UI

Specifically, this is a 3 node cluster provisioned following the Rancher AWS Quickstart: https://rancher.com/docs/rancher/v2.x/en/quick-start-guide/deployment/amazon-aws-qs/

## Setup

* Within gitlab go to the Project or Group to add the cluster
* Navigate to `Operations > Kubernetes`
* Click `Add Kubernetes cluster`
* Click `Add existing cluster
* We can see there are 3 pieces of information we need from our cluster
    * API URL
    * CA Certificate
    * Service Token

### API URL

There are likely serveral entries in the config file with a URL. The first one does not seem to work. I have had success using the second entry. For me, it looks like `server: "https://xx.xx.xx.xx:6443"`. The first entry looks entry that I could not get to work looks like `server: "https://3.91.34.242/k8s/clusters/c-p4tdl"`.

### CA Certificate

The CA Cert is the `certificate-authority-data` field for the same entry. The spacing and base64 decoding make it slightly less straight forward. You can copy the context (between the quotes and paste in the one liner below):

```
> echo "<paste data here>" | tr -d ' ' | base64 -d
```

### Service Token

Nothing special here. Just copy the token value.

### Setup Runner

Go to the `Applications` tab under the cluster config page. Install tiller, and then install the Runner chart.

Navigate in Gitlab `Project > Settings > CI/CD > Runners`. Then disable shared runners, and enable the runner from the cluster.

