## K8s quickstart

The suggested starting point for getting started within kubernetes is listed below. This are merely recommendations, and do not need to be followed for successful path to being deployable on kubernetes.

1. Have something containerized that you want to deploy
1. Push to some registry that the cluster can access
1. Write deployment manifests to wrap the details of the deployment
1. Expose with with a service and load balancer
1. Understand Service Mesh benefits

### Workflow

The following diagram illustrates a good starting point for preparing your application to deploy on the kubernetes environment. It starts by creating a container, and moving that application through the necessary steps for defining the kubernetes manifests and helm chart. After that is deployed and functioning, introducing a service mesh to adhere to a Zero Trust model.

![](images/workflow_diagram.png)

### Containerize

The first step is to understand how to containerize your application so that is stateless and horizontally scalable. Some common pitfall's we've seem outlined below.

* Do not put too many services in one container
  * Proxies, databases, queues, etc are their own containers
* In order to be capable of horizontal scaling, applications should be stateless units
* Bootstrap as much as possible in the Dockerfile (not during each `docker run`)
  * Kubernetes does have init containers, that can help this flow when needed
* Consider database migrations (zero downtime migrations are an added +)
* Avoid using anything that uses dockers --privileged flags
* Scan containers for vulnerabilities with a tool like anchore (starting with a well known base image should help, e.g. alpine)

### Deploy to Kubernetes

I recommend deploying sample applications first.

The `gcr.io/google-samples/hello-app:1.0` image is a simple place to start to understand how services and load balancers work in kubernetes without worrying about dealing with issues in your container.

Either a cloud hosted cluster of k3d is our recommendation for first time usage in an internet connected environment.

### Service Mesh

Next install a service mesh and understand the capabilities. Istio is the selection here.

```
$ istioctl manifest apply --set profile=demo
```

The palomar project is meant as a simple 3 service project that is tailored to exploring this concept.
https://gitlab.com/mfilipiak-ccg/palomar
