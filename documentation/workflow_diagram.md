
``` mermaid
stateDiagram

state Containerize {
    services: Identify Services
    dockerfile: Create Service Dockerfiles
    build: Build Docker Images" as build
    test: Test docker image locally
    push: Push to private docker registry

    services --> dockerfile
    dockerfile --> build
    build --> test
    test --> push
}

state Install_k8s_Tools {
    kubectlInstall: Install kubectl
    helmInstall: Install helm

    kubectlInstall --> helmInstall
}

state Create_HelmChart {
    state "Create k8s deployment manifest for image" as deployment
    state "Create k8s service manifest for deployment" as service
    state "Create helm chart for manifests" as helm

    deployment --> service
    service --> helm
}

state Create_Dev_Cluster {
    state "Install k3d" as k3dInstall
    state "Create k3d cluster" as k3dCreateCluster
    state "Connect to cluster" as connect
    state "Helm install dependency services" as dependencies

    k3dInstall --> k3dCreateCluster
    k3dCreateCluster --> connect
    connect --> dependencies
}

state Create_Rancher_Cluster {
    state "Deploy Rancher cluster to hosts" as deploy
    state "Connect to cluster" as connect
    state "Helm install dependency services" as dependencies

    deploy --> connect
    connect --> dependencies
}

state DeployToCluster {
    state "Helm deploy" as deploy
    state "Test Deployment" as test

    deploy --> test
}

state Introduce_Service_Mesh {
    state "Install Istio" as install
    state "Deploy Istio for mTLS" as mtls
    state "Configure helm chart for mesh" as config
    state "Modify application for OpenTelemetry" as telemetry
    
    install --> mtls 
    mtls --> config
    config --> telemetry
}

Containerize --> Install_k8s_Tools
Install_k8s_Tools --> Create_HelmChart
Create_HelmChart --> Create_Dev_Cluster
Create_HelmChart --> Create_Rancher_Cluster
Create_Dev_Cluster --> DeployToCluster
Create_Rancher_Cluster --> DeployToCluster
DeployToCluster --> Introduce_Service_Mesh
Introduce_Service_Mesh --> DeployToCluster
```