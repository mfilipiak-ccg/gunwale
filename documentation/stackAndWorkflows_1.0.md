# Technology Stack and Workflows
September 2019

[TOC]

This document outlines the selected technologies and stack involved in the DevSecOps pipeline and architecture runtime. The selections are focused on rapid development and integration, as well as supporting security, monitoring, and scaling out of the box for vendor integration onto the platform.


## Justification

The selections are largely based on the industry accepted solutions. There is a concerted effort to utilize focused components that are cloud agnostic. This is done to acknowlege that the best practices in the DevOps community are moving fast, and the solution to a specific portion of this stack are likely to change. The idea is to position each solution to be replacable in isolation without affecting the entire system.

The stack is corroborated and/or pulled from the working being done by DSOP.
https://dccscr.dsop.io/dsop
https://www.youtube.com/watch?v=lLrIwwLG3Yo

## Deployment/Runtime Stack

Let's start by looking at a contrived end state of the deployment.

```graphviz
digraph {
    // allow edges between clusters
    compound=true;
    // allow rank=same to reference other clusters
    newrank=true;
    size=11;

    node [shape=record, style="filled,rounded", fillcolor="#ffffff"]

    subgraph cluster_k8s {
        label="Kubernetes Cluster"

        subgraph cluster_shared {
            label="Platform Services"
            subgraph cluster_auth {
                label="Auth Namespace"
                style=filled;
                fillcolor="#FFBF94";
                Authentication
                OPA [label="OPA Server"]
            }

            subgraph cluster_monitoring {
                label="Monitoring Namespace";
                style=filled;
                fillcolor="#FFBF94";

                prometheus [label="Prometheus", shape=cylinder];
                grafana [label="Grafana"];
                jaeger [label="Jaeger"];
                prometheus -> grafana

                EFK [label="ELK/EFK"];
            }
        }
        OPA -> EFK [style=invis]

        subgraph cluster_core_services {
            label="Common Service Namespace";

            messaging [label="{Messaging|-Envoy\l|-OPA\l}", fillcolor="#54C9FF"];
            vmManagement [label="{VM Management|-Envoy\l|-OPA\l}", fillcolor="#54C9FF"];
            userManagement [label="{User Management|-Envoy\l|-OPA\l}", fillcolor="#54C9FF"];
            fluentd_core [label="Fluentd Daemon", shape=component, fillcolor="#FFBF94"]
        }
        subgraph cluster_vendor1 {
            label="Vendor1 Namespace"
            style=filled;
            fillcolor="#CCC62F"

            c2Server_v1 [label="{C2 Server|-Envoy\l|-OPA\l}", fillcolor="#54C9FF"]
            taskServer_v1 [label="{Task Server|-Envoy\l|-OPA\l}", fillcolor="#54C9FF"]
            redis_v1 [label="Redis",shape=cylinder, fillcolor="#54C9FF"]
            taskServer_v1 -> redis_v1
            c2Server_v1 -> redis_v1
            fluentd_v1 [label="Fluentd Daemon", shape=component, style=filled, fillcolor="#FFBF94"]
        }

        messaging -> prometheus [ltail=cluster_core_services];
        messaging -> OPA [ltail=cluster_core_services];
        c2Server_v1 -> prometheus [ltail=cluster_vendor1];
        c2Server_v1 -> OPA [ltail=cluster_vendor1];
        fluentd_core -> EFK
        fluentd_v1 -> EFK
    }

}
```
```graphviz
digraph {
    rankdir="LR"
    size=4;
    ranksep=0;

    node [shape=record, style="filled,rounded", fillcolor="#ffffff"]
    subgraph cluster_legend { 
        label = "Legend";
        key_pod [label="Application Pods", fillcolor="#54C9FF"]
        key_provided [label="Provided Infrastructure", fillcolor="#FFBF94"]
        key_vendor [label="Vendor Deployment", fillcolor="#CCC626"]
        key_pod -> key_provided -> key_vendor [style=invis]
    }
}
```

The majority of the built in security and features are accomplished using sidecars. There are two main sidecars, but additional sidecars can be deployed to handle additional features.

Envoy
: Feature rich sidecar that handle the majority of the features
  * Mutual TLS and service mesh ACLs
  * HTTP & gRPC proxy
  * Hooks for custom handling inside the proxy
  * Inspects traffic to provide unintrusive opentracing and authorization

OPA
: Focused on authorization
  * Validates JWT tokens
  * Receives OPA policies from the server
  * Sidecar saves trips to the centralized OPA management server
  * Validates OPA policies for HTTP access

```graphviz
digraph {
    // allow edges between clusters
    compound=true;
    // allow rank=same to reference other clusters
    //newrank=true;
    size=11;
    rankdir="TB"
    nodesep=1

    node [shape=record, style="filled,rounded", fillcolor="#ffffff"]

    User
    subgraph cluster_node {
        label = "K8S Node"

        fluentd [label="Fluentd DaemonSet", shape=component, fillcolor="#FFBF93"]

        subgraph cluster_pod {
            label="Example Pod"
            rankdir="TD"
            style=filled;
            fillcolor="#CCC626"

            Webserver [label="Webserver"]
            Envoy [label="Envoy Sidecar", fillcolor="#FFBF93"]
            OPA_SC [label="OPA Sidecar", fillcolor="#FFBF93"]

            Envoy -> OPA_SC
            OPA_SC -> Envoy
            Envoy -> Webserver
            Webserver -> Envoy

        }

        subgraph cluster_shared {
            label="Platform Services"
            subgraph cluster_monitoring {
                label="Monitoring Namespace";
                style=filled;
                fillcolor="#FFBF94";

                prometheus [label="Prometheus", shape=cylinder];
                grafana [label="Grafana"];
                jaeger [label="Jaeger"];
                prometheus -> grafana

                EFK [label="ELK/EFK"];
            }
            subgraph cluster_auth {
                label="Auth Namespace"
                style=filled;
                fillcolor="#FFBF94";
                Authentication
                OPA [label="OPA Server"]
            }

        }
        OPA_SC -> OPA
    }

    User -> Envoy [label="GET", color=blue]
    //User -> Authentication [label="GET", color=blue]
    Envoy -> User [label="200", color=blue]
    Envoy -> prometheus [color=darkgreen]
    Envoy -> Authentication [color=blue]
    Envoy -> jaeger [color=darkgreen]
    Webserver -> fluentd [label="stdout/stderr", color=darkgreen]
    fluentd -> EFK
}
```

## Core Runtime Features

The table below outlines the main features of the platform, and the contributing components of those features. The 'Intrusive' column specifies if the feature requires changes to existing code to implement (Provided/Low/Medium/High).

| Feature | Enabling Technology | Description | Usage Requirements | Intrusive Score |
| :--- | :--- | :--- | :--- | :--- | :--- |
| Deployment | Helm | Helm packages define the deployment into kubernets | Each system will need to define their service as a helm/kubernetes package. Scaffolding scripts will be available to provide a skeleton for common deployment approaches | Medium
| Emit Logs | Fluentd | Sending logs to an emitter | Log to stdout/stderror from docker automatically gets slurped up from daemon set fluentd service. Configure fluentd for other log files. Fluentd will also parse common log formats (syslog, nginx, flask, etc) | Very Low
| View Logs | ELK/EFK | View/Filter within Kibana | None | Provided
| Emit Tracing | Envoy/Istio | Tracing data shows call tree/heirachy information as calls traverse the system. Envoy supports HTTP & gRPC tracing out of the box. Will emit to Jaeger. | Coarse grained service to service communications require no extra code per service. Fine grained tracing can be added to a service using opentracing and sending it to Jaeger. External REST calls will need to forward headers to preserve tracing spans | Provided-Low
| View Tracing | Jaeger | Visualize historical call trees, latencies, and other metadata | None | Provided
| Authentication | OPA/JWT | Validate credentials and issue tokens | None. Sidecars will proxy stateless/zero trust tokens and rely on JWT. Custom authZ can be done in addition if needed. | Provided
| Authorization | OPA | Complex policies can be injested into the system to define readable and maintainable authorization systems. | Custom policies will need to be packaged in deliverable. Helpers available for application or URL based policies. | Low
| Horizontal Scaling | Kubernetes | Allows scaling of services in the cluster by running behind a load balancer and scaling according to load | Requires code to be written with minimal to no shared state within the service process. | Medium 
| AutoScaling | Istio/Kubernetes | Horizontal scaling based on load metrics | None | Provided
| Hardening | OPA | Ensures services in the cluster are complaint to a set of rules | Need to remediate any finding to be deployed | Medium
| Canary Releases | Helm/Kubernetes | Test production releases without impacting mission. Also possible to do A/B or blue/green deployments | None | Provided

## Pipeline

The pipeline has several phases, all of which feed into a single consolidated pipeline. The production pipeline will assemble the overall release and run tests on them. The results of which will automatically make available a new development environment for the development teams. They can (and should) be retrieved frequently to avoid any drift from the baseline. 

```puml
@startuml
hide empty description
scale 600 width
scale 600 height
skinparam state {
    BackgroundColor<<Release>> LimeGreen
    BorderColor Gray
    ArrowColor Black
}

[*] -> Tasking
Tasking -> Development

state Development {
    [*] -> DevComplete
    DevComplete -> InternalPipeline
    InternalPipeline -> Packaging
}

Packaging --> Delivery
state Delivery {
    state "Verify Signature" as sig

    [*] --> Transfer
    [*] --> Shipment
    Shipment --> sig
    Transfer --> sig
    sig --> Git
    sig --> Nexus
}
Nexus --> Pipeline: Trigger

state Pipeline {
    state "Resolve Dependencies" as Dep
    state "Scan source" as ScanSrc
    state "Scan containers" as ScanCnt
    state "Deploy Test Env" as TestEnv

    Dep -> ScanSrc
    ScanSrc -> Build
    Build -> ScanCnt
    ScanCnt --> TestEnv
    TestEnv -> Test
    Test -> Report
    Report -> Teardown
}

state "Jira/Confluence" as Jira
Report --> Jira : Tag/Document Results
Pipeline --> Delivery : Tag Artifacts
Pipeline ---> Release <<Release>>

note right of Delivery: Artifacts & versions are tagged and tracked

@enduml
```

### Vendor Pipeline

Development environments will be setup to include deployment and configuration of the tools used in the official pipeline. This will enable all vendors to validate their products as they are doing development. This will eliminate surprises when submitting to the the official pipeline (preventing delays in release). 

In some cases, certain tools will need special licenses. In those cases, it will be up the vendor whether they need licenses, or skip those stages in the pipeline.

Delivery from a vendor will
* Sign the artifacts with their key
* Deliver source (into centralized Git)
    * Will be scanned with static analysis tools and create a report
* Any deployment artifacts
    * Docker containers
    * Tool packages
    * Helm Charts
    * Build scripts
* Any tests to be run on the integrated deployment
    * Testing of dependencies/assumptions about the environment
    * Testing integrated functionality

## Distributable Environment

Being able to distribute a development environment outside of the lab is a key capability to enable vendor development groups to work against a realistic environment.

* Develop against a representative environement, eliminating surprises (and reveal misleading APIs/documents)
* Integration in the lab is reduced, as vendors can integrate on-site
* Sensitive or hardware components are emulated
* Eliminate replicating features that are provided by the framework on-site

```dot
digraph {
    size = 9
    subgraph {
        rankdir="LR";
        newrank=true;
        subgraph cluster_k8s {
            label="Cluster";

            node [shape=record, style="fill,rounded", fillcolor="#54C9FF"]
            StandinService [label="Lanency Injecting Service", shape=diamond, style=filled, fillcolor="#dddddd"]
            Service2 -> StandinService
            Service1 -> StandinService
            StandinService -> Service3
            StandinService -> Service4
        }
    }
    subgraph {
        rankdir="LR";
        newrank=true;
        subgraph cluster_real {
            label="Test/Production Cluster";
            fill="#ddffdd"

            node [shape=record, style="fill,rounded", fillcolor="#54C9FF"]
            RealService [label="Real Service", shape=diamond, style=filled, fillcolor="#dddddd"]
            Service2_pr [label="Service2"]
            Service1_pr [label="Service1"]
            Service3_pr [label="Service3"]
            Service4_pr [label="Service4"]
            Service1_pr -> RealService
            Service2_pr -> RealService
            RealService -> Service4_pr
            RealService -> Service3_pr
        }
    }
}
```

### Delivery

Not all environments will be the same, depending on the vendor and environments they will be deployed on.

|Environment | Services | Level |
| :--- | :--- | :---
| Full | All (Testing/Production)  | S
| Core | Open Source Env  | U
| Etc...

<span style="color: darkred">Need to determine how to cut/distribute releases to vendors</span>
