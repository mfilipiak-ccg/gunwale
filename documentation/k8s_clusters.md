## Kubernetes Clusters

### Hosted Cluster

The various cloud hosted services seem like very easy ways to get started with Kubernetes, since all the major components are integrated in. Of course, there are costs associated with it.

Currently, GKE offers credits (and additional credits can be acquired through GitLab). Given that, and the beginner friendliness, GKE may be a great place to get started.

* Google Kubernetes Engine (GKE)
* Amazon Elastic Kubernetes Service (EKS)
* Azure Kuberetes Service (AKS)

### Installed Cluster

Again, a lot of options out there. For an easy to install option, Rancher has been our go to.  We have written a terraform/ansible based vSphere deployer that 

Rancher Website:
https://rancher.com/

We have been using a Rancher deployment on AWS, using plain EC2 instances. This is a cheaper option than EKS, and mimics the development environment more closely.
https://rancher.com/docs/rancher/v2.x/en/quick-start-guide/deployment/amazon-aws-qs/

#### Cloud

We have a version that wires in a private registry under:
[{repo}/deployment/rancher_aws](../deployment/rancher_aws/README.md)

#### ESXi

See offline scripts targeted at ESXi.

[Offline Rancher Deploy](../deployment/offline/README.md)

### Development Cluster

* k3d
* docker (on Mac)
* minikube
* microk8s

We have had success exploring kubernetes on all of these flavors. Personally, I have switched most of ephemeral cluster testing to use k3d, because it is quick to create and teardown. It does not seem to be as much of a memory hog as other solutions I have tried.

Here is a video demonstration of k3d:
https://youtu.be/jUPL4ZOlJ0E

### Recommendations

Beware that different kubernetes products will have different components embedded in them, and they may behave differently. For that reason, it is recommended to try to deploy the core services you will be using in production to ensure you are compatible with them.  

For example, you may want to disable the cluster's packaged load balancer solution (e.g. nginx or traefik), and deploy and utilize Istio's load balancer. However, the most important part is utilizing the kuberenetes architecture for load balancing. Keeping load balancing and proxying as a separate layer will allow swapping that solution down the road, and being relatively agnostic to the solution that is provided.