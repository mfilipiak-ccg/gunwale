# Gunwale

Consult the latest recommendations from CNCF for a good starting point for the accepted standard for a specific technology.

https://www.cncf.io/   
https://landscape.cncf.io/

See the quickstart for our suggested familization path: [k8s quickstart](k8s_quickstart.md)

## Kubernetes deployment

The first item of consideration is selecting kubernetes provider. There are many viable options, some easier to deploy than others. This list is based on what we have experience using, and would recommend based on being able to deploy and use easily.

There is a tradeoff between using a production ready hosted cluster, where you have little control over, and an installed cluster that is portable but you have more responsibility over all the components (e.g. external Load Balancer IPs, and data storage).

**Read more:** [K8S Cluster Information](k8s_clusters.md)

## Service Mesh

We have primarily been focused on utilizing Istio for the Service Mesh technology. This helps in configuring and enforcing Zero Trust architectures, as well as various telemetry features.

This project explores the features in Istio (currently at v1.5).

https://istio.io/

Here is a video (and accompanying project) on Istio Autorization and implementing Zero Trust.
https://youtu.be/-h9NzJqzWeY  
https://gitlab.com/mfilipiak-ccg/palomar


## Build Pipeline

Deploying and integrating the applications from various sources benefit from delivery conventions and controls. One area of exploration is the common static analysis, deployment, and integration testing phases of the CI/CD environment.

We have put together prototypes of these ideas into Jenkins, and GitLab workflows.

## Security

Below are a list of services that will help support the security of the cluster as it is running. In addition to anything checked in the pipeline before it enters the cluster.

* Istio Auth, mTLS
* [Falco Runtime Security](https://falco.org/)
* [OPA](https://www.openpolicyagent.org/)
* [OPA Gatekeeper](https://github.com/open-policy-agent/gatekeeper)

