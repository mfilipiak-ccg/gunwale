SHELL := /bin/bash
GUNWALE_NS = default
DOCKER_TAG = alpha

build-dir:
	mkdir -p build

namespaces:
	kubectl create namespace istio-system ||:
	kubectl create namespace opa-istio ||:
	kubectl create namespace $(GUNWALE_NS) ||:
	# sidecar injection
	kubectl label namespace $(GUNWALE_NS) istio-injection=enabled --overwrite
	kubectl label namespace $(GUNWALE_NS) opa-istio-injection="enabled" --overwrite

core: namespaces build-dir
	# TODO make these creds dynamic
	#export CREDENTIALS_USERNAME=admin
	#export CREDENTIALS_PASSPHRASE=admin
	cd build && git clone https://github.com/istio/istio.git ||:
	cd build/istio && helm template install/kubernetes/helm/istio-init --name istio-init --namespace istio-system | kubectl apply -f -
	cd build/istio && helm template install/kubernetes/helm/istio --name istio --namespace istio-system --values install/kubernetes/helm/istio/values-istio-demo.yaml --set kiali.enabled=true | kubectl apply -f -

opa: namespaces
	kubectl apply -f deployment/gunwale/auth/opa_quick_start.yaml 

delete-deployment:
	kubectl -n $(GUNWALE_NS) delete po,svc --all
	kubectl delete namespace $(GUNWALE_NS) istio-system opa-istio ||:
	helm delete --purge gunwale

##
# Gunwale
###
gunwale-delete:
	helm delete --purge gunwale ||:
	kubectl delete namespace $(GUNWALE_NS) ||:

gunwale-deploy: helminit namespaces core opa
	kubectl create clusterrolebinding permissive-binding \
		--clusterrole=cluster-admin \
		--user=admin --user=kubelet \
		--group=system:serviceaccounts ||:
	kubectl create -f deployment/gunwale/rbac.yaml ||:
	# helm won't do this for us if we remove a dependency, so do it ourselves (risk is if one is statically copied into the charts dir)
	rm -f deployment/gunwale/charts/*
	cd deployment/gunwale && helm dependency update
	helm install --namespace $(GUNWALE_NS) --name gunwale deployment/gunwale --set CI_COMMIT_SHORT_SHA=$(DOCKER_TAG) --set CI_COMMIT_REF_NAME=$(GUNWALE_NS)

gunwale-upgrade:
	helm upgrade --namespace $(GUNWALE_NS) gunwale deployment/gunwale --set CI_COMMIT_SHORT_SHA=alpha --set CI_COMMIT_REF_NAME=devel

build-docker-images:
	cd core-ui && docker build -t core-ui:$(DOCKER_TAG) .
	cd vm-creation && docker build -t vm-creation:$(DOCKER_TAG) .

jenkins-password:
	kubectl get secret --namespace pipeline pipeline-jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode; echo
